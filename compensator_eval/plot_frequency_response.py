#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import pdb
from pdb import set_trace
import win32ui

# columns of the test params file
FREQ = 0
START = 1
END = 2
SETTLE = 3
N = 4

def main():
       
    # load the log 
    dlg = win32ui.CreateFileDialog(True, None, None, 0, None)
    dlg.SetOFNTitle("Select hl_controller log file")
    dlg.DoModal()
    logfile  = dlg.GetPathName()
    t, u_a, y, e_roll, ticks = np.loadtxt(logfile, delimiter=',', skiprows=1,
                           usecols=(0, 8, 10, 18, 21), unpack=True)
    
    # transform 'elapsed' into absolute time (seconds since the epoch)
    logstart = os.stat(logfile).st_ctime
    t_abs = np.array(t)
    t_abs += logstart
    
    # load the test params file that will help us extract individual test
    # runs from the log
    dlg = win32ui.CreateFileDialog(True, None, None, 0, None)
    dlg.SetOFNTitle("Select the test params file")
    dlg.DoModal()
    test_params_logfile_path = dlg.GetPathName()
    test_params = np.loadtxt(test_params_logfile_path, delimiter=',', skiprows=1)
             
    # extract stimulus and response at each frequency
    stimuli = []
    responses = []
    for param_set in test_params:
        start_idx = 0
        end_idx = 0
        start_time = param_set[START] + param_set[SETTLE]
        
        # we want the beginning of the time record to correspond with the
        # start of a sine wave, so find the next positive-going zero crossing
        for i in range(len(t_abs)):
            if start_time > t_abs[i]:
                start_idx = i
                end_idx = start_idx + param_set[N]
        stimulus = u_a[start_idx : end_idx]
        stimuli.append(stimulus)
        response = y[start_idx : end_idx]
        responses.append(response)
        pdb.set_trace()
        
    p0 = plt.plot(t, u_a, label='u_a')
    p1 = plt.plot(t, y, label='y')
    plt.legend([p0, p1], ['u_a', 'y'])
    plt.show()

    #plt.figure(2)
    #p3 = plt.plot(t_abs[start_idx : end_idx], u_a[start_idx : end_idx], label='u_a')
    #p4 = plt.plot(t_abs[start_idx : end_idx], y[start_idx : end_idx], label='y')
    #plt.legend([p3, p4], ['u_a', 'y'])
    #plt.show()

if __name__ == "__main__":
    main()

## code graveyard
    #p0 = plt.plot(t, u_a, label='u_a')
    #p1 = plt.plot(t, y, label='y')
    #p2 = plt.plot(t, e_roll, '^', label='e_roll')
    #plt.legend([p0, p1, p2], ['u_a', 'y', 'e_roll'])
