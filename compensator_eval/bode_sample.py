#!/usr/bin/env python
# -*- encoding: utf-8 -*-
#
# Note: no idea where I found this --RDA
#
import matplotlib
matplotlib.use('TkAgg')
import scipy.signal
from pylab import *
rcParams['text.usetex'] = 0
rcParams['figure.dpi'] = 96
 
def line_properties(event):
    epsilon = 3
    def _line_distances(line):
        trans = line.get_transform()
        x = line.get_xdata(valid_only = True)
        y = line.get_ydata(valid_only = True)
        xdata, ydata = trans.numerix_x_y(x, y)
        distances = sqrt((event.x-xdata)**2 + (event.y-ydata)**2)
        return distances.min(), x[distances.argmin()], y[distances.argmin()]
        
    def _select_line(line, x, y):
        ax = line.axes
        for t in ax.texts:
            tx, ty = t.get_position()
            if sqrt((x-tx)**2 + (y-ty)**2) < epsilon:
                ax.texts.remove(t)
                for l in ax.lines:
                    if len(l.get_xdata()) != 1: continue
                    lx, ly = l.get_xdata()[0], l.get_ydata()[0]
                    if sqrt((x-lx)**2 + (y-ly)**2) < epsilon:
                        ax.lines.remove(l)
                        break
                gcf().canvas.draw()
                return
        ax.text(x, y, '(%g, %g)' %(x, y), fontsize='10')
        ax.plot([x], [y], 'o')
        gcf().canvas.draw()
            
    for ax in gcf().axes:
        for line in ax.get_lines():
            min, x, y = _line_distances(line)
            if min < epsilon:
                _select_line(line, x, y)
                return
                
def tf(*args, **kwargs):
    if len(args) == 3:
        zeros = asarray(args[0])
        poles = asarray(args[1])
        gain = args[2]
        G = scipy.signal.lti(zeros, poles, gain)
    elif len(args) == 2:
        B, A = args
        G = scipy.signal.lti(B, A)
    G.desc = kwargs.get('desc', '')
    return G
 
def bode(G):
    figure()
#    w=arange(1e-4j, 1e-1j, 1e-6j)
    w=arange(1e-4, 1e-1, 1e-6) * complex(0, 1)
    y = polyval(G.num, w) / polyval(G.den, w)
    mag = 20.0*log10(abs(y))
#    pha = arctan2(y.getimag(), y.getreal())*180.0/pi
    pha = arctan2(y.imag, y.real)*180.0/pi
    for i in arange(1, pha.shape[0]):
        if abs(pha[i]-pha[i-1]) > 179:
            pha[i:] -= 360.
 
    subplot(211)
    semilogx(w.imag, mag)
    grid()
    gca().xaxis.grid(True, which='minor')
    
    gcf().text(0.5, 0.91, r'Bode diagram: %s' %G.desc, 
               horizontalalignment='center', fontsize=16)    
    ylabel(r'Magnitude (db)')
    
    subplot(212)
    semilogx(w.imag, pha)
    grid()
    gca().xaxis.grid(True, which='minor')
    ylabel(r'Phase (deg)')
    yticks(arange(0, pha.min()-30, -30))
 
    gcf().canvas.mpl_connect('button_press_event', line_properties)
 
    return mag, pha
 
def impulse(G):
    clf()
    plot(*G.impulse())
    grid()
    title('Impulse response: %s' %G.desc)
    xlabel('Time (sec)')
    ylabel('Amplitude')
 
def step(G):
    clf()
    plot(*G.step())
    grid()
    title('Step response: %s' %G.desc)
    xlabel('Time (sec)')
    ylabel('Amplitude')
    
 
G = tf([], [-1./227]*2, 1./227**2)
G.desc = r'$G_{H1}(s)=\frac{1}{(1+227s)^2}$'
 
bode(G)
show()
#savefig('h1_bode.ps')
 
 
impulse(G)
#savefig('h1_impulse.ps')
 
step(G)
#savefig('h1_step.ps')
 
#
G = tf([], [-1./530]*3, 1./530**3)
G.desc = r'$G_{H2}(s)=\frac{1}{(1+530s)^3}$'
 
bode(G)
show()
#savefig('h2_bode.ps')
 
impulse(G)
#savefig('h2_impulse.ps')
 
step(G)
#savefig('h2_step.ps')