#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import matplotlib
from matplotlib import pyplot as plt
import numpy as np
import pdb
from pdb import set_trace
from time import time
from time import localtime
import win32ui

from common.ring_buffer import RingBuffer
from common import quadconfig
from common.sleep import Sleeper
from HL_controller.client.controller_client import QuadClient
from HL_controller.client.controller_client import QuadObserver

def create_log_file():
        t = localtime()
        logfilename = '%4d' % t.tm_year + \
                      '%02d' % t.tm_mon + \
                      '%02d' % t.tm_mday + \
                      '_' + \
                      '%02d' % t.tm_hour + \
                      '%02d' % t.tm_min + \
                      '_freq_response_test_log' + \
                      '.txt'
                                
        logfile = open(logfilename, 'w')
        columns = \
            "frequency," \
            "start," \
            "end\n"
        
        logfile.write(columns)
        return logfile
        
def inject_signal(quad_client, frequency, amplitude, duration):
    
    start = time()
    quad_client.inject_sinewave((frequency, amplitude))
    sleeper.sleep(duration * 1e6)
    end = time()
    quad_client.inject_sinewave((1.0, 0.0))
    return start, end

######## TEST CONFIGURATION ############
#
#
SINE_AMPLITUDE_DEGREES = 10.0

# Wescott claims that "a minimum of 1000 points will give good data". 
N = 1000

# How long do we have to run tests?
SESSION_DURATION_S = 300.0

def main():
    
    global sleeper
    sleeper = Sleeper()
    logfile = create_log_file()        

    # How many test runs will we do, and how long will each run be?
    f_s = quadconfig.CONTROL_LOOP_RATE_HZ
    test_duration = N / f_s 
    test_run_count = np.floor(SESSION_DURATION_S / test_duration)
    
    # Figure out possible test frequencies. We will only use frequencies that
    # give an integer number of cycles over the length of the test.
    # We start with one cycle per test duration, working up to half of the
    # sample rate.
    test_freqs_hz = []
    cycles_per_test = 1.0
    done = False
    while done == False:
        test_freq_hz = cycles_per_test / test_duration

        if test_freq_hz >= (f_s / 2.0):
            done = True
        else:
            test_freqs_hz.append(test_freq_hz)
            cycles_per_test += 1
        
    test_freqs_hz = np.array(test_freqs_hz)
    test_freqs_hz = test_freqs_hz[np.arange(0, len(test_freqs_hz), int(len(test_freqs_hz) / test_run_count))]
    if(len(test_freqs_hz)) > test_run_count:
        test_freqs_hz = test_freqs_hz[:len(test_freqs_hz)-1]
        
    test_freqs_hz = np.array([1.0, 2.0])
    print 'test freqs: '
    print test_freqs_hz
    
    class TestParamSet:
        pass
    test_params = []        
    for freq in test_freqs_hz:
        tps = TestParamSet()
        tps.frequency = freq
        tps.amplitude = SINE_AMPLITUDE_DEGREES
        tps.duration = test_duration
        test_params.append(tps)

    # initialize the quad
    qc = QuadClient('localhost', 4444, 4445)
    qc.connect()
    qc.send_control_msg(0.0, 0.0, 0.0, 128.0)
    print 'QuadClient qc connected and sent hover throttle command.'
    print 'Pausing for five seconds.'
    sleeper.sleep(5e6)
    
    # run the tests
    for tps in test_params:
        
        print 'Injecting ' + str(tps.frequency) + \
            ' Hertz for ' + str(tps.duration) + ' seconds...'
        start, end = inject_signal(qc, tps.frequency, tps.amplitude,
            tps.duration)
        log_entry = str(tps.frequency) + ',' + str(start) + ',' + \
            str(end) + '\n'
        logfile.write(log_entry)

        print 'Pausing for five seconds.'
        sleeper.sleep(5e6)
    
    print 'Cutting throttles and disconnecting.'
    qc.send_control_msg(0.0, 0.0, 0.0, 0.0)
    qc.disconnect()
    logfile.close()
     
        
if __name__ == "__main__":
    main()











