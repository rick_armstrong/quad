#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import cPickle
import matplotlib
from matplotlib import pyplot as plt
from numpy import mean
from numpy import absolute
from numpy import array
from numpy import diff
from numpy import std
import pdb
from pdb import set_trace
import sys
from time import time
from time import localtime

from common.ring_buffer import RingBuffer
from common import quadconfig
from common.airframe_state import AirframeState
from common.sleep import Sleeper
from HL_controller.client.controller_client import QuadClient
from HL_controller.client.controller_client import QuadObserver
from test_params import pid_list
from test_params import make_test_params
from compensator_eval.summarize_results_file import summarize_results


ROLL_LIMIT_DEG = 45.0
ROLL_LIMIT_INF_DEG = 360.0
READY_ROLL_DEG = 20.0
MAX_DEVIATION_DEG = 1.5
MAX_STD_DEVIATION_DEG = 3.0
SETTLE_TIMEOUT_S = 10.0

class Error(Exception):
    pass

class TravelLimitExceededError(Error):
    def __init__(self, quad_observer):
        self.qo = quad_observer
    pass

class SettleTimeoutError(Error):
    def __init__(self, quad_observer):
        self.qo = quad_observer
    

def wait_for_zero_crossing(qo, timeout_s):
    wait = True
    start = time()
    history = []
    while(wait):
        if((time() - start) > timeout_s):
            raise SettleTimeoutError(qo)
        
        #roll_history = qo.airframe_state_roll.get_all()
        history += list(qo.get_all())
        if(len(history) < 2): continue
        roll_history = [x['roll'] for x in history]
        if not (min(roll_history) < 0 and max(roll_history) > 0): continue
        break    
        
def wait_for_settle(qo, angle, travel_limit, timeout_s):
    wait = True
    start = time()
    history = []
    while(wait):
        
        history += list(qo.get_all())
        if(len(history) < 2): continue
        roll_history = [x['roll'] for x in history]
        if((time() - start) > timeout_s):
            raise SettleTimeoutError(qo)
        
        # if the quad freaks out, throw
        if max(absolute(roll_history)) > travel_limit:
            raise TravelLimitExceededError(qo)
        
        rh = array(roll_history)
        if len(rh) > 218: # three seconds
            if std(rh[-218:-1]) < MAX_STD_DEVIATION_DEG:
                print "Quad settled at ", angle, " degrees commanded"
                wait = False
        sleeper.sleep(250000)    
        
def run_tests():
    raise NotImplementedError

if __name__ == "__main__":
    
    if False:
        run_tests()
        print ""
        print "All tests passed."
        sys.exit(0)
        
    sleeper = Sleeper()
    
    qc = QuadClient('192.168.4.4', 4444, 4445)
    qc.connect()
    
    qo = QuadObserver(5)
    qo.start()
    sleeper.sleep(1e6)

    results = []
    #pid_list = make_test_params()
    n = 1
    for test_pid in pid_list:
        print ""
        print "Starting trial " + str(n) + " of " + str(len(pid_list)) + \
            " with PID of ", test_pid
        n += 1
        stable_pid = (0.5, 0.0, 0.2)
        print "Setting stable PID of ", stable_pid, " throttle to hover power"
        qc.set_PID(stable_pid)
        qc.send_control_msg(0.0, 0.0, 0.0, 128)
        print "Pausing for five seconds..."
        sleeper.sleep(5e6)
        try:
            print "Pausing for settle into ready position"
            qc.send_control_msg(0.0, 0.0, READY_ROLL_DEG, 128)
            sleeper.sleep(5e6)
                
            print "Setting test PID and waiting for settle"
            qo.get_all()
            qc.set_PID(test_pid)
            wait_for_settle(qo, READY_ROLL_DEG, ROLL_LIMIT_DEG,
                            SETTLE_TIMEOUT_S)
            
            print "Initiating step and waiting for settle"
            qo.get_all()
            start_t = time()
            qc.send_control_msg(0.0, 0.0, 0.0, 128)
            wait_for_zero_crossing(qo, SETTLE_TIMEOUT_S)
            rise_time = time() - start_t
            wait_for_settle(qo, 0.0, ROLL_LIMIT_DEG, SETTLE_TIMEOUT_S)
                        
            # results format v1.0
            history = qo.get_all()
            roll_history = []
            for entry in history:
                roll = entry["roll"]
                roll_history.append(roll)
            
            results.append((test_pid,
                            rise_time,
                            time() - start_t,
                            roll_history))
            
        except TravelLimitExceededError, e:
            qc.send_control_msg(0.0, 0.0, 0.0, 0)
            print "******Travel limit exceeded, cutting throttles."
            results.append((test_pid, -1.0, -1.0,
                            [x['roll'] for x in e.qo.get_all()]))
            sleeper.sleep(1e6)
            
        except SettleTimeoutError, e:
            qc.send_control_msg(0.0, 0.0, 0.0, 0)
            print "******Settle time limit exceeded, cutting throttles."
            results.append((test_pid, -1.0, -1.0,
                            [x['roll'] for x in e.qo.get_all()]))
            sleeper.sleep(1e6)

    print "End of test. Shutting down..."
    qc.set_PID(stable_pid)
    qc.send_control_msg(0.0, 0.0, 0.0, 128)
    qc.send_control_msg(0.0, 0.0, 0.0, 0)
    qc.disconnect()
    print "test complete"

    # pickle the results
    t = localtime()
    logfilename = '%4d' % t.tm_year + \
                  '%02d' % t.tm_mon + \
                  '%02d' % t.tm_mday + \
                  '_' + \
                  '%02d' % t.tm_hour + \
                  '%02d' % t.tm_min + \
                  '_step_responses' + \
                  '.pkl'
                            
    logfile = open(logfilename, 'wb')
    cPickle.dump(results, logfile)
    logfile.close()
    summarize_results(logfilename)
    
    
    
    
    
    
    
    
    
    
    
    
    
        