#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import pickle
import win32ui


def summarize_results(results_file):

    # load the pickled results
    f = open(results_file, 'rb')
    results = pickle.load(f)
    f.close()

    # open a text file to contain the summary
    summary_file_name = os.path.splitext(results_file)[0] + '.txt'
    f = open(summary_file_name, 'w')
    
    
    columns = 'P,I,D,rise_time,settle_time\n'
    f.write(columns)
    
    for result in results:
        P = result[0][0]
        I = result[0][1]
        D = result[0][2]
        rise_time = result[1]
        settle_time = result[2]
        entry = str(P) + ',' + str(I) + str(',') + str(D) + ',' \
            + str(rise_time) + ',' + str(settle_time) + '\n'
        f.write(entry)
        
    f.close()
    
if __name__ == "__main__":

    dlg = win32ui.CreateFileDialog(True, None, None, 0, None)
    dlg.DoModal()
    results_file = dlg.GetPathName()
    
    