#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import pdb
from pdb import set_trace
import win32ui

def main():
    dlg = win32ui.CreateFileDialog(True, "", "", 0, "")
    dlg.SetOFNTitle("Select an hl_controller log file")
    dlg.DoModal()
    logfile_path = dlg.GetPathName()
    
    # get column indices
    f = open(logfile_path, 'r')
    first_line = f.readline()
    f.close()
    cols = first_line.split(",")
    
    t, d_roll, p_roll, roll, e_roll, throttle_roll_offset = np.loadtxt(
        logfile_path, delimiter=',', skiprows=1,
                           usecols=(cols.index("elapsed"),
                                    cols.index("d_roll"),
                                    cols.index("p_roll"),
                                    cols.index("roll"),
                                    cols.index("e_roll"),
                                    cols.index("throttle_roll_offset")),
                           unpack=True)

    p0 = plt.plot(t, d_roll, label='d_roll')
    p1 = plt.plot(t, p_roll, label='p_roll')
    p2 = plt.plot(t, roll, label='roll')
    p3 = plt.plot(t, e_roll, '-+', label='e_roll')
    p4 = plt.plot(t, throttle_roll_offset, label='throttle_roll_offset')
    plt.legend()
    plt.show()
    
if __name__ == "__main__":
    main()