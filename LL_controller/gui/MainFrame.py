#Boa:Frame:MainFrame
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import wx
import serial
import array
import struct

#from common import throttleControl
from common.throttleControl import set_throttle
from common import quadconfig


def create(parent):
    return MainFrame(parent)

[wxID_MAINFRAME, wxID_MAINFRAMEBTNCANCEL, wxID_MAINFRAMEBTNSEND, 
 wxID_MAINFRAMESLIDER1, wxID_MAINFRAMESLIDER2, wxID_MAINFRAMESLIDER3, 
 wxID_MAINFRAMESLIDER4, wxID_MAINFRAMETEXTCTRL1, wxID_MAINFRAMETEXTCTRL2, 
 wxID_MAINFRAMETEXTCTRL3, wxID_MAINFRAMETEXTCTRL4, 
] = [wx.NewId() for _init_ctrls in range(11)]

class MainFrame(wx.Frame):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_MAINFRAME, name='MainFrame',
              parent=prnt, pos=wx.Point(538, 300), size=wx.Size(379, 250),
              style=wx.DEFAULT_FRAME_STYLE, title='Low-level Throttle Control')
        self.SetClientSize(wx.Size(371, 221))
        self.Bind(wx.EVT_CLOSE, self.OnMainFrameClose)

        self.slider1 = wx.Slider(id=wxID_MAINFRAMESLIDER1, maxValue=255,
              minValue=0, name='slider1', parent=self, pos=wx.Point(8, 8),
              size=wx.Size(296, 24), style=wx.SL_HORIZONTAL, value=0)
        self.slider1.SetMax(255)
        self.slider1.Bind(wx.EVT_SCROLL, self.OnSlider1Scroll)

        self.slider2 = wx.Slider(id=wxID_MAINFRAMESLIDER2, maxValue=100,
              minValue=0, name='slider2', parent=self, pos=wx.Point(8, 40),
              size=wx.Size(296, 24), style=wx.SL_HORIZONTAL, value=0)
        self.slider2.SetMax(255)
        self.slider2.Bind(wx.EVT_SCROLL, self.OnSlider2Scroll)

        self.slider3 = wx.Slider(id=wxID_MAINFRAMESLIDER3, maxValue=100,
              minValue=0, name='slider3', parent=self, pos=wx.Point(8, 72),
              size=wx.Size(296, 24), style=wx.SL_HORIZONTAL, value=0)
        self.slider3.SetMax(255)
        self.slider3.Bind(wx.EVT_SCROLL, self.OnSlider3Scroll)

        self.slider4 = wx.Slider(id=wxID_MAINFRAMESLIDER4, maxValue=100,
              minValue=0, name='slider4', parent=self, pos=wx.Point(8, 104),
              size=wx.Size(296, 24), style=wx.SL_HORIZONTAL, value=0)
        self.slider4.SetMax(255)
        self.slider4.Bind(wx.EVT_SCROLL, self.OnSlider4Scroll)

        self.textCtrl1 = wx.TextCtrl(id=wxID_MAINFRAMETEXTCTRL1,
              name='textCtrl1', parent=self, pos=wx.Point(312, 8),
              size=wx.Size(48, 21), style=0, value='0')

        self.textCtrl2 = wx.TextCtrl(id=wxID_MAINFRAMETEXTCTRL2,
              name='textCtrl2', parent=self, pos=wx.Point(312, 40),
              size=wx.Size(48, 21), style=0, value='0')

        self.textCtrl3 = wx.TextCtrl(id=wxID_MAINFRAMETEXTCTRL3,
              name='textCtrl3', parent=self, pos=wx.Point(312, 72),
              size=wx.Size(48, 21), style=0, value='0')

        self.textCtrl4 = wx.TextCtrl(id=wxID_MAINFRAMETEXTCTRL4,
              name='textCtrl4', parent=self, pos=wx.Point(312, 104),
              size=wx.Size(48, 21), style=0, value='0')

        self.btnSend = wx.Button(id=wxID_MAINFRAMEBTNSEND, label='Send',
              name='btnSend', parent=self, pos=wx.Point(264, 168),
              size=wx.Size(75, 23), style=0)
        self.btnSend.Bind(wx.EVT_BUTTON, self.OnBtnSendButton,
              id=wxID_MAINFRAMEBTNSEND)

        self.btnCancel = wx.Button(id=wxID_MAINFRAMEBTNCANCEL, label='Cancel',
              name='btnCancel', parent=self, pos=wx.Point(160, 168),
              size=wx.Size(75, 23), style=0)
        self.btnCancel.Bind(wx.EVT_BUTTON, self.OnBtnCancelButton,
              id=wxID_MAINFRAMEBTNCANCEL)

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.throttle1 = 0
        self.throttle2 = 0
        self.throttle3 = 0
        self.throttle4 = 0
        self.port = serial.Serial(
            quadconfig.LL_CONTROLLER_PORT_IDX, 
            quadconfig.LL_CONTROLLER_PORT_BAUD,
            timeout=quadconfig.LL_CONTROLLER_PORT_TIMEOUT_s) 
        set_throttle([0, 0, 0, 0], self.port)
        self.port.read(6)
        
    def OnMainFrameClose(self, event):
        self.port.close()
        event.Skip()

    def OnSlider1Scroll(self, event):
        self.textCtrl1.ChangeValue(str(self.slider1.GetValue()))
        event.Skip()

    def OnSlider2Scroll(self, event):
        self.textCtrl2.ChangeValue(str(self.slider2.GetValue()))
        event.Skip()

    def OnSlider3Scroll(self, event):
        self.textCtrl3.ChangeValue(str(self.slider3.GetValue()))
        event.Skip()

    def OnSlider4Scroll(self, event):
        self.textCtrl4.ChangeValue(str(self.slider4.GetValue()))
        event.Skip()

    def OnBtnSendButton(self, event):
        self.throttle1 = self.slider1.GetValue()
        self.throttle2 = self.slider2.GetValue()
        self.throttle3 = self.slider3.GetValue()
        self.throttle4 = self.slider4.GetValue()

        set_throttle(
            [self.throttle1, self.throttle2, self.throttle3, self.throttle4],
            self.port)
        response = self.port.read(6)
        print str(response)
        event.Skip()

    def OnBtnCancelButton(self, event):
        self.slider1.SetValue(self.throttle1)
        self.textCtrl1.ChangeValue(str(self.throttle1))

        self.slider2.SetValue(self.throttle2)
        self.textCtrl2.ChangeValue(str(self.throttle2))

        self.slider3.SetValue(self.throttle3)
        self.textCtrl3.ChangeValue(str(self.throttle3))

        self.slider4.SetValue(self.throttle4)
        self.textCtrl4.ChangeValue(str(self.throttle4))

        event.Skip()
                


        
