//	Copyright (c) 2012, Rick Armstrong
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//			modification, are permitted provided that the following conditions are met:
//
//	* Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//
//	* Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	* Neither the name of the copyright holder nor the names of its
//	contributors may be used to endorse or promote products derived from
//	this software without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//			IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
//			FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//			DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//			SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
//			CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <avr/io.h>		// include I/O definitions (port names, pin names, etc)
#include <avr/interrupt.h>	// include interrupt support
#include <string.h>

#include "global.h"		// include our global settings
#include "uart2.h"		// include uart function library
#include "rprintf.h"	// include printf function library
#include "a2d.h"		// include A/D converter function library
#include "timer.h"		// include timer function library (timing, PWM, etc)
#include "servo.h"		// include RC servo driver library
#include "vt100.h"		// include VT100 terminal library

#include "checksum.h"

#define MOTOR_COUNT 4

typedef struct CommandPacket_tag
{
	char	throttle[MOTOR_COUNT];
	u16		checksum;
} CommandPacket;


int main(void)
{
	
	// timer
	timerInit();

	// a2d
	a2dInit();
	a2dSetPrescaler(ADC_PRESCALE_DIV32);
	a2dSetReference(ADC_REFERENCE_AVCC);
	DDRF = 0x00;	// set up PORTF as A2D input
	PORTF = 0x00;	// enable pullups 
	
	// servo
	servoInit();	
	servoSetChannelIO(0, _SFR_IO_ADDR(PORTB), PB5);
	servoSetChannelIO(1, _SFR_IO_ADDR(PORTB), PB6);
	servoSetChannelIO(2, _SFR_IO_ADDR(PORTB), PB7);
	servoSetChannelIO(3, _SFR_IO_ADDR(PORTE), PE3);
	
	DDRB = 0xF0; 	// set PORTB PWM (and LED pin) to output
	PORTB |= 0x10;	// turn off yellow LED
	DDRE = 0xFF; 	// set all PORTE pins to otuput	
	
	// uart2
	uartInit();
	uartSetBaudRate(1, 57600); // default baud rate is 9600

	// start with motors off
	for(u08 motor = 0; motor < MOTOR_COUNT; motor++)
	{
		servoSetPosition(motor, 0);
	}


	 CommandPacket cmd;
	 memset(&cmd, 0, sizeof(CommandPacket));
	 while(1)
	 {
	 	// read a command packet
	 	while(uartReceiveBufferIsEmpty(1)){}
	 	timerPause(1); // wait long enough to get the whole packet
	 	u08* pCmd = &cmd;			
	 	for(u16 i = 0; i < sizeof(CommandPacket); i++)
	 	{ 
	 		*(pCmd + i) = uart1GetByte();
	 	}
	 	
	 	// echo the command
	 	uartSendBuffer(1, pCmd, sizeof(CommandPacket));

	 	// verify the command
	 	if(cmd.checksum != cksum(pCmd, 4))
	 	{
	 		// turn on the yellow warning LED 
	 		// and ignore this command
	 		PORTB &= ~(0x10);
	 		continue;
	 	}
	 	
	 	for(u08 motor = 0; motor < MOTOR_COUNT; motor++)
	 	{
	 		servoSetPosition(motor, cmd.throttle[motor]);
	 	} 
	}
	 

	
	return 0;
}