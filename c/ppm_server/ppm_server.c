/****************************************************************************
*
*   Copyright (c) 2006 Richard Armstrong <rick@radiantflux.com>
*
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License version 2 as
*   published by the Free Software Foundation.
*
*   Alternatively, this software may be distributed under the terms of BSD
*   license.
*
*   See README and COPYING for more details.
*
****************************************************************************
* Reads events from kernel module "gpio-event", written by Dave Hylands.
* This program is based on Dave Hylands' sample app.
*
****************************************************************************/

/* ---- Include Files ---------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include "gpio-event-drv.h"

/* channel ranges (ms) */
#define CH1_MIN (0.5 * 1e-3)
#define CH2_MIN (0.5 * 1e-3)
#define CH3_MIN (0.6 * 1e-3)
#define CH4_MIN (0.5 * 1e-3)
#define CH5_MIN (0.5 * 1e-3)
#define CH6_MIN (0.5 * 1e-3)
#define CH1_MAX (1.5 * 1e-3)
#define CH2_MAX (1.5 * 1e-3)
#define CH3_MAX (1.4 * 1e-3)
#define CH4_MAX (1.5 * 1e-3)
#define CH5_MAX (1.5 * 1e-3)
#define CH6_MAX (1.5 * 1e-3)

/* function prototypes */
void sigint_handler(int signum);
void sigsegv_handler(int signum);

/* globals */
FILE                *fd_gpio;
FILE                *fd_out;
#define             LOG_BUFFER_LEN 357000 // 60 seconds * 50Hz * 7 pulses * 17 chars/pulse
char                logBuf[LOG_BUFFER_LEN];
float               ch[6];
int                 sfd;
unsigned LOG_ONLY = 0;
unsigned logBufferFull = 0;
const float MAX_TILT_COMMAND_DEGREES = 20.0;
const float MAX_YAW_COMMAND_THROTTLE = 100.0;
const float YAW_DEADBAND = 25.0;
const float TILT_DEADBAND_DEGREES = 2.0;

typedef enum {
    ROLL,
    PITCH,
    COLLECTIVE,
    YAW,
    KNOB_R,
    KNOB_L
} Axis_t;

double range[6] = {
    (CH1_MAX-CH1_MIN),
    (CH2_MAX-CH2_MIN),
    (CH3_MAX-CH3_MIN),
    (CH4_MAX-CH4_MIN),
    (CH5_MAX-CH5_MIN),
    (CH6_MAX-CH6_MIN)
};

double rangeMin[6] = {
    CH1_MIN,
    CH2_MIN,
    CH3_MIN,
    CH4_MIN,
    CH5_MIN,
    CH6_MIN,    
};

/****************************************************************************
*
*  main
*
***************************************************************************/
void sigint_handler(int signum)
{
   printf("Caught ctrl-c...closing socket, writing data to file and exiting.\n");
   
   /* close our connection to hl_controller */
   if(!LOG_ONLY)
   {
        shutdown(sfd, SHUT_RDWR);
        close(sfd);    
   }
   
   /* write our data */
   fd_out = fopen("data_out", "w");
   printf("strlen(logBuf) = %d\n", strlen(logBuf));
   fwrite(logBuf, 1, strlen(logBuf), fd_out);   
   fclose(fd_out);
   fclose(fd_gpio);
   exit(signum);
}

int main(int argc, char **argv)
{
    if(2 == argc)
    {
        if(0 == strcmp("-l", argv[1]))
        {
            LOG_ONLY = 1;
        }
    }
    
    // clear our log buffer
    memset(logBuf, 0, sizeof(logBuf));
    
    if(!LOG_ONLY)
    {
        // connect to hl_controller
        struct addrinfo hints;
        struct addrinfo *result;
        int s;
        memset(&hints, 0, sizeof(struct addrinfo));
        hints.ai_family = AF_INET;    /* Allow IPv4 or IPv6 */
        hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
        hints.ai_flags = 0;
        hints.ai_protocol = 0;          /* Any protocol */
    
        s = getaddrinfo("192.168.4.4", "4444", &hints, &result);
        if (s != 0) {
            fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
            exit(EXIT_FAILURE);
        }
        sfd = socket(result->ai_family, result->ai_socktype, 0);
        printf("connecting to hl_controller...\n");
        int rc = connect(sfd, result->ai_addr, result->ai_addrlen);
        if(-1 == rc)
        {
            fprintf(stderr, "connect() failed: %s\n", gai_strerror(rc));
            exit(EXIT_FAILURE);
        }
        printf("...connected\n");        
    }
            
   // Register signal and signal handler
   signal(SIGINT, sigint_handler);
    
    if ((fd_gpio = fopen("/dev/gpio-event", "r")) == NULL)
    {
        perror("Check to make sure gpio_event_drv has been loaded. Unable to open /dev/gpio-event");
        exit(1);
    }
    ioctl(fileno(fd_gpio), GPIO_EVENT_IOCTL_SET_READ_MODE, 1);

    unsigned curChan = 0;
    unsigned loopCount = 0;
    char     *curPos = logBuf;
    unsigned notified = 0;
    while (1)
    {
        ssize_t numBytes = 0;
        char    argStr[60];
        argStr[0] = '\0';

        GPIO_Event_t    gpioEvent_prev;
        GPIO_Event_t    gpioEvent_cur;        
        if((numBytes = fread(
                              &gpioEvent_cur,
                              1,
                              sizeof(gpioEvent_cur),
                              fd_gpio
                              )
            ) == sizeof(gpioEvent_cur))
        {
            snprintf(
                     argStr, sizeof(argStr), "%ld.%06ld\n",
                     gpioEvent_cur.time.tv_sec,
                     gpioEvent_cur.time.tv_usec
                     );

            if((curPos - logBuf + strlen(argStr)) > LOG_BUFFER_LEN)
            {
                if(!notified)
                {
                    printf("logbuffer full, further log data will be discarded.\n");
                }
                notified = 1;
            }
            else
            {
                char    *tmp = argStr;
                while((*curPos = *tmp))
                {
                    curPos++;
                    tmp++;
                }
            }
        }
        else
        {
            if (numBytes > 0)
            {
                fprintf(
                        stderr,
                        "Read unexpected number of bytes: %d, expecting %d\n",
                         numBytes,
                         sizeof(gpioEvent_cur)
                         );
            }
        }
        if(0 == gpioEvent_prev.gpio) /* need two pulses to start with */
        {
            gpioEvent_prev = gpioEvent_cur;
            continue;
        }

        /* wait for a sync pulse */        
        double t0 = gpioEvent_prev.time.tv_sec + ((double)gpioEvent_prev.time.tv_usec / 1e6);
        double t1 = gpioEvent_cur.time.tv_sec + ((double)gpioEvent_cur.time.tv_usec / 1e6);
        float pulseWidth = t1 - t0 - 0.0005;
        if(pulseWidth > 0.010)
        {
            /* sync pulse received, get ready to read data pulses */                 
            curChan = 0;
            gpioEvent_prev = gpioEvent_cur;            
            continue;           
        }
        else
        {
            switch(curChan)
            {
                case ROLL: 
                {
                    ch[curChan] = (pulseWidth - range[curChan]) * 2.0 * 1000.0 * MAX_TILT_COMMAND_DEGREES;
                    if(TILT_DEADBAND_DEGREES > abs(ch[curChan]))
                    {
                        ch[curChan] = 0.0;
                    }
                    break;
                }
                
                case PITCH: 
                {
                    ch[curChan] = (pulseWidth - range[curChan]) * 2.0 * 1000.0 * MAX_TILT_COMMAND_DEGREES;
                    if(TILT_DEADBAND_DEGREES > abs(ch[curChan]))
                    {
                        ch[curChan] = 0.0;
                    }
                    break;
                }

                case COLLECTIVE: 
                {
                    ch[curChan] = ((pulseWidth - rangeMin[curChan]) / range[curChan]) * 255.0;
                    break;
                }
                
                case YAW: 
                {
                    ch[curChan] = -1.0 * (pulseWidth - range[curChan]) * 2.0 * 1000.0 * MAX_YAW_COMMAND_THROTTLE;
                    if(YAW_DEADBAND > abs(ch[curChan]))
                    {
                        ch[curChan] = 0.0;
                    }
                    break;
                }

                case KNOB_R: 
                {
                    ch[curChan] = ((pulseWidth - rangeMin[curChan]) / range[curChan]) * 255.0;
                    break;
                }

                case KNOB_L: 
                {
                    ch[curChan] = ((pulseWidth - rangeMin[curChan]) / range[curChan]) * 255.0;
                 
                    if(!LOG_ONLY)
                    {
                        // last channel; send
                        float msg[4] = {ch[YAW], ch[PITCH], ch[ROLL], ch[COLLECTIVE]};
                        send(sfd, &msg, sizeof(msg), 0);                        
                    }
                    break;
                }

                
                default:
                {
                    printf("WARNING...ignoring spurious pulse. Pulses this frame: %d, pulseWidth: %lf\n", curChan + 2, pulseWidth);
                    ++curChan;
                    gpioEvent_prev = gpioEvent_cur;
                    continue;
                }            
            }
        
            gpioEvent_prev = gpioEvent_cur;
            if(0 == (++loopCount % 250))
            {
                printf("%3.1f %3.1f %3.1f %3.1f %3.1f %3.1f\n",
                       (float)ch[0],
                       (float)ch[1],
                       (float)ch[2],
                       (float)ch[3],
                       (float)ch[4],
                       (float)ch[5]
                       );
            }
            ++curChan;
        }
    }

    fclose(fd_gpio);
    exit(0);
    return 0;

} // main


