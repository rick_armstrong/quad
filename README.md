# quad
Code, documentation, and test artifacts from a scratch-built quadrotor project. Managed to get it flying shortly before they became available at Walmart for $100.
Some of the library code was gleaned from online sources, and copyright was left intact when it existed.

https://youtube.com/playlist?list=PLOhQJG1JL-iHw9VU36T9jOKRDhrPCinBH&si=aVkiCQ4EAxQc2fV7

![System Diagram](https://bitbucket.org/rick_armstrong/quad/raw/dd54582e7df621567e5c44da08d0b1f763ae6efa/doc/201310_quad_computers_and_software.png)
