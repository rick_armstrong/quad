#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import numpy as np
from numpy import array
from numpy import dot
from pdb import set_trace
import time
from visual import *

from common.sleep import Sleeper
    
### Simulated PowerPlant + IMU
class DynamicState:
    def __init__(self):
        
        # roll, pitch, yaw (world frame)
        self.phi = 0.0
        self.phi_dot = 0.0
        self.theta = 0.0
        self.theta_dot = 0.0       
        self.psi = 0.0
        self.psi_dot = 0.0

        # roll, pitch, yaw  (body frame)
        self.p = 0.0
        self.q = 0.0 
        self.r = 0.0
        self.p_dot = 0.0
        self.q_dot = 0.0
        self.r_dot = 0.0
        
class QuadSim:
    def __init__(self, log=False):
        
        # logfile for logging internals
        self.log = log
        if log == True:
            t = time.localtime()
            logfilename = '%4d' % t.tm_year + \
                          '%02d' % t.tm_mon + \
                          '%02d' % t.tm_mday + \
                          '_' + \
                          '%02d' % t.tm_hour + \
                          '%02d' % t.tm_min + \
                          '.quadsim_internals.txt'
                             
            self.columns = ""       
            self.logfile = open(logfilename, 'w')
        
        # set some display window attributes
        scene.x = 1200
        scene.y = 0
        scene.width = 600
        scene.height = 600
        scene.forward = (-1, -1, 0)
        floor = curve(pos=[(-2, 0, -2), (2, 0, -2), (2, 0, 2), (-2, 0, 2),
            (-2, 0, -2)])    
        
        # draw the airframe with a red motor arm marking "front"
        self.airframe = frame(pos=(0, 0.5, 0))
        self.axis_pitch = cylinder(frame=self.airframe, pos=(0, 0, -0.5),
                                   axis=(0, 0, 1), length=1.0, radius=0.01)
        self.axis_roll_f = cylinder(frame=self.airframe, pos=(0, 0, 0),
                                    axis=(1, 0, 0), length=0.5, radius=0.01,
                                    color=color.red)
        self.axis_roll_r = cylinder(frame=self.airframe, pos=(0, 0, 0),
                                    axis=(-1, 0, 0), length=0.5, radius=0.01)
        self.MOTOR_DIA_MIN = 0.1
        self.motor_1 = ring(frame=self.airframe, pos=(0.5, 0, 0),
                            axis=(0, 1, 0), radius=self.MOTOR_DIA_MIN)
        self.motor_2 = ring(frame=self.airframe, pos=(0, 0, -0.5),
                            axis=(0, 1, 0), radius=self.MOTOR_DIA_MIN)
        self.motor_3 = ring(frame=self.airframe, pos=(-0.5, 0, 0),
                            axis=(0, 1, 0), radius=self.MOTOR_DIA_MIN)
        self.motor_4 = ring(frame=self.airframe, pos=(0, 0, 0.5),
                            axis=(0, 1, 0), radius=self.MOTOR_DIA_MIN)

        #
        # model and simulation parameters
        #
        self.dt = 0.01
        
        # from GRASP lab model
        self.k_f = 6.11e-8
        self.rpm_max = 7800.0
        
        # converts a vector of throttle commands (collective, roll_offset,
        # pitch_offset, yaw_offset) to rotor speeds.
        self.A_throttle_to_w = array([
            (1, 0, -1, 1),
            (1, 1, 0, -1),
            (1, 0, 1, 1),
            (1, -1, 0, -1)]) * self.rpm_max / 255.0
        
        # motor arm length
        self.L = 1.0
        
        # inertia matrix (using old numbers from previous sim)
        self.I = 0.2 * eye(3)
        
        # mass (kg)
        self.mass = 0.756     
        
        # motor speeds
        self.w_i = dot(self.A_throttle_to_w, array([0.0, 0.0, 0.0, 0.0]).T)
        
        # dynamic motion state
        self.current_state = DynamicState()
        self.last_state = DynamicState()
    
    def __del__(self):
        if self.log == True:
            self.logfile.close()
            
    def log_entry(self):
        logdata = {
            "elapsed" : time.clock(),
            "w_0" : self.w_i[0],
            "w_1" : self.w_i[1],
            "w_2" : self.w_i[2],
            "w_3" : self.w_i[3]
        }
        
        # write first line of the log file, if we haven't already
        if len(self.columns) == 0:
            for key in logdata.keys():
                self.columns += (key + ",")
            self.columns += "\n"
            self.logfile.write(self.columns)
            
        # write the log entry
        logstring = ""
        for key in logdata.keys():
            logstring += str(logdata[key]) + ","
        self.logfile.write(logstring + "\n")
    
    def clamp(self, a, mini, maxi):
        for i in range(len(a)):
             a[i] = min(a[i], maxi)
             a[i] = max(a[i], 0)
        
    def set_throttle(self,
                    yaw_offset,
                    pitch_offset,
                    roll_offset,
                    collective):
        
        throttles = array([collective,
                              roll_offset,
                              pitch_offset,
                              yaw_offset])
        self.w_i = dot(self.A_throttle_to_w, throttles.T)
        self.clamp(self.w_i, 0, 7800.0)
        
        # update the graphic
        self.motor_1.radius = self.MOTOR_DIA_MIN + \
                              0.2 * self.w_i[0] / self.rpm_max
        self.motor_2.radius = self.MOTOR_DIA_MIN + \
                              0.2 * self.w_i[1] / self.rpm_max
        self.motor_3.radius = self.MOTOR_DIA_MIN + \
                              0.2 * self.w_i[2] / self.rpm_max
        self.motor_4.radius = self.MOTOR_DIA_MIN + \
                              0.2 * self.w_i[3] / self.rpm_max
        
    def update(self):
        
        # roll axis
        self.current_state.p_dot = self.k_f * (self.w_i[1]**2 - self.w_i[3]**2) / self.I[0, 0]
        p_dot_bar = (self.last_state.p_dot + self.current_state.p_dot) / 2.0
        delta_p = p_dot_bar * self.dt
        self.current_state.p = self.last_state.p + delta_p
        p_bar = (self.last_state.p + self.current_state.p) / 2.0
        delta_roll = p_bar * self.dt
        self.airframe.rotate(angle=delta_roll)
        
        # pitch axis
        self.current_state.q_dot = self.k_f * (self.w_i[0]**2 - self.w_i[2]**2) / self.I[1, 1]
        q_dot_bar = (self.last_state.q_dot + self.current_state.q_dot) / 2.0
        delta_q = q_dot_bar * self.dt
        self.current_state.q = self.last_state.q + delta_q
        q_bar = (self.last_state.q + self.current_state.q) / 2.0
        delta_pitch = q_bar * self.dt
        self.airframe.rotate(angle=delta_pitch)

        self.last_state = self.current_state
        
        if self.log == True:
            self.log_entry()

    def ypr(self):
        rate(50)
        self.update()
        
        yaw = -atan2(self.airframe.axis[2], self.airframe.axis[0])
        if yaw < 0:
            yaw += 2*pi
        pitch = atan2(self.airframe.axis[1], self.airframe.axis[0])
        roll = atan2(self.airframe.up[2], self.airframe.up[1])
        
        ticks = time.clock()
        return (degrees(yaw), degrees(pitch), degrees(roll), 0.0)

if __name__ == '__main__':
    
    sim = QuadSim()
    roll_offset = 0.0
    loop_count = 0

    while(1):
        if scene.kb.keys:
            s = scene.kb.getkey()
            if s == 'y':
                axis = sim.airframe.up
                sim.airframe.rotate(angle=0.0872664, axis=axis)
                
            if s == 'p':
                axis = cross(sim.airframe.axis, sim.airframe.up)
                sim.airframe.rotate(angle=0.0872664, axis=axis)

            if s == 'r':
                axis = sim.airframe.axis
                sim.airframe.rotate(angle=0.0872664, axis=axis)

        sim.ypr()
        if (loop_count % 20) == 0:
            print "ypr: ", sim.ypr()
        loop_count += 1

#from math import sin
#from math import cos
#from OpenGL.GLUT import *
#from OpenGL.GLU import *
#from OpenGL.GL import *
#from pdb import set_trace
#from pymunk import Vec2d
#from pymunk.util import *
#from pymunk import *
#import serial
#import time
#import threading
#from threading import Lock
#from threading import Thread
#from common import quadconfig
#
#SLEEP_TICKS = 16
#
#class QuadSimView(Thread):
#    def __init__(self, sim):
#        self.model = sim
#        Thread.__init__(self)
#
#    def display(self):
#        # center pivot
#        glClear(GL_COLOR_BUFFER_BIT)
#        glLoadIdentity()
#        glColor3f(0.0, 0.0, 0.0)        
#        glBegin(GL_POINTS)
#        glColor3f(0.0, 0.0, 1.0)
#        glVertex2f(0.0, 0.0)
#        glEnd()
#        
#        # chassis outline
#        y, p, r, t = self.model.ypr()
#        glPushMatrix()
#        glRotatef(r, 0.0, 0.0, 1.0)
#        glBegin(GL_LINE_LOOP)
#        for v in self.model.chassis_verts:
#            glVertex2f(v.x, v.y)
#        glEnd()
#        glPopMatrix()
#        glutSwapBuffers()
#        self.model.update()
#        
#    def timercall(self, value):
#        glutTimerFunc(SLEEP_TICKS, self.timercall, 0)	
#        glutPostRedisplay()
#        
#    def run(self):
#        glutInit(sys.argv)
#        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA)	
#        glutInitWindowSize(320, 240)
#        glutCreateWindow("Boogie on down!!!")
#        glClearColor(1.0, 1.0, 1.0, 0.0)
#        glPointSize(3.0)
#        glMatrixMode(GL_PROJECTION)
#        glLoadIdentity()
#        glOrtho(-1000.0, 1000.0, -750.0, 750.0, -1.0, 1.0)
#        glScalef(2.0, 2.0, 1.0)
#        glMatrixMode(GL_MODELVIEW)
#        glutDisplayFunc(self.display)
#        glutTimerFunc(SLEEP_TICKS, self.timercall, 0)
#        glutMainLoop()
#        
#### Simulated PowerPlant + IMU
#class QuadSim:
#    def __init__(self):
#        self.lock = Lock()
#        self.roll_offset = 0.0
#        
#        # set up the pymunk simulation
#        init_pymunk()
#        self.space = Space()
#        self.space.gravity = Vec2d(0.0, -900.0)
#        self.staticBody = Body(inf, inf)
#        self.chassis_verts = [
#                        Vec2d(-100,-18),
#                        Vec2d(100, -18),
#                        Vec2d(100, 18),
#                        Vec2d(-100,18)
#                        ]
#        
#        # these numbers from measurements as of 11/14/2009
#        chassis_mass = 0.756 # kg
#        #chassis_moment = 0.2 # kg * m^2
#        chassis_moment = 0.1
#        self.chassis = Body(chassis_mass, chassis_moment)
#        self.chassis.position = Vec2d(100.0, 800.0)
#        self.space.add(self.chassis)
#        shape = Poly(self.chassis, self.chassis_verts, Vec2d(0,0))
#        shape.friction = 0.0
#        self.space.add(shape)        
#        
#        self.view = QuadSimView(self)
#        self.view.start()
#        
#        self.loop_count = 0
#        
#    def update(self):
#        substeps = 3
#        dt = (1.0/60.0) / substeps
#        for i in range(substeps):
#            self.chassis.reset_forces()
#            
#            # max torque = 560g max thrust * 38cm^2
#            self.chassis.torque = ((self.roll_offset / 255) * 560.0 * 0.1444)
#            self.space.step(dt)
#
#    def set_throttle(self,
#                    yaw_offset,
#                    pitch_offset,
#                    roll_offset,
#                    collective):
#        
#        self.roll_offset = roll_offset        
#                
#    def ypr(self):
#        yaw = sin(time.clock())
#        pitch = cos(time.clock())
#        roll = self.chassis.angle / 3.1415 * 180
#        return (yaw, pitch, roll, 0.0)
        

