#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import array
from collections import deque
from copy import copy
from cPickle import loads, dumps
import cProfile
import math
from math import sqrt
from math import sin
from math import cos
import numpy as np
import os
import select
import serial
import socket
import struct
from struct import Struct
import sys
import time
import threading
#import winsound
#import win32api,win32process,win32con
import pdb

from common.airframe_state import AirframeState
from common import quadconfig
from common import sleep
from common import throttleControl
from common.control_command import ControlCommand
from common.quadconfig import LOG_BROADCAST_ADDR, LOG_BROADCAST_PORT, \
    LOG_BROADCAST_LISTEN_IF, COMMAND_SERVER_LISTEN_IF
from common.ring_buffer import RingBuffer
from power_plant import PowerPlant

CFG_MGSID_PID_PARAMS = 0
CFG_MSGID_INJECT_SINE = 1

QUAD_UDP_BROADCAST_FMT = 'fffffffffffff'
QUAD_UDP_BROADCAST_LEN = 52

CMD_QUEUE_LEN = 4

def same_sign(lhs, rhs):
    lhs_sign = 0
    rhs_sign = 0
    
    if lhs >= 0:
        lhs_sign = 1
    else:
        lhs_sign = -1
        
    if rhs >= 0:
        rhs_sign = 1
    else:
        rhs_sign = -1
    
    return (lhs_sign * rhs_sign) > 0

class CommandServer(threading.Thread):
    def __init__(self, port, packet_len):
        self.command_queue = deque()
        self.sleeper = sleep.Sleeper()
        self.port = port
        self.packet_len = packet_len
        self.quit = False
        threading.Thread.__init__(self)

    def __del__(self):
        self.quit = True
        
    def run(self):
        socket.setdefaulttimeout(quadconfig.COMMAND_SERVER_SOCKET_TIMEOUT_s)
        print 'CommandServer: binding new listener socket'
        self.socket = socket.socket(
            socket.AF_INET,
            socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((COMMAND_SERVER_LISTEN_IF, self.port))
        self.socket.listen(1)
        clientConnected = False
        
        while((self.quit == False)):
            while(clientConnected == False):
                try:
                    (connection, address) = self.socket.accept()
                    print 'CommandServer: new connection accepted on port: ', self.port
                    clientConnected = True
                except socket.timeout:
                    if(self.quit == True):
                        break
                    continue
            if(self.quit == True):
                break    

            readable, writable, err = select.select(
                [connection],
                [connection],
                [],
                1)
            
            # do we have something to read?
            if len(readable) != 0:                        
              cmd = ''
              while len(cmd) < self.packet_len:
                  chunk = connection.recv(self.packet_len-len(cmd))
                  if chunk == '':
                    print 'CommandServer: remote connection terminated'
                    clientConnected = False
                    break
                  cmd = cmd + chunk
              
              if len(cmd) == self.packet_len:
                print 'CommandServer: commmand received'
                self.command_queue.append(cmd)
            
            self.sleeper.sleep(quadconfig.CONTROL_LOOP_PERIOD_S * 1e6)
                
        print "CommandServer: exiting"
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()
        del self.socket
    
    def in_waiting(self):
        return len(self.command_queue)
    
    def get_command(self):
        return self.command_queue.popleft()
    
class IMU:
    def __init__(self):
        self.sleeper = sleep.Sleeper()
    
    def ypr(self):
        sleeper.sleep(0.05 * 1e6)
        return (0.0, 0.0, 0.0, 0.0, 0)

class BadChecksumError(Exception):
    def __init__(self, checksum, sum):
        self.checksum = checksum
        self.sum = sum
    
class MicrostrainIMU(IMU):
    def __init__(self):
        IMU.__init__(self)
        self._port = serial.Serial(
            quadconfig.IMU_PORT_STR,
            quadconfig.IMU_PORT_BAUD,
            timeout=quadconfig.IMU_PORT_TIMEOUT_s)
        self.flushed = False
        
    def __del__(self):
        self._port.close()

    def ypr(self):
        if not self.flushed:
            self._port.flushInput()
            self.flushed = True
            
        while(chr(quadconfig.IMU_PACKET_HEADER) != self._port.read(1)):
            pass
        packet = chr(quadconfig.IMU_PACKET_HEADER) + self._port.read(22)
        response = struct.pack('B', 0) + packet
        unpacked = struct.unpack('>hhhhhhhhhhHh', response)
        roll = unpacked[1] * 360.0 / 65535.0
        pitch = unpacked[2] * 360.0 / 65535.0
        yaw = unpacked[3] * 360.0 / 65535.0
        roll_rate = unpacked[7] / (32768000.0 / 10000.0) * 180.0 / 3.1415
        pitch_rate = unpacked[8] / (32768000.0 / 10000.0) * 180.0 / 3.1415
        yaw_rate = unpacked[9] / (32768000.0 / 10000.0) * 180.0 / 3.1415
        ticks = int(unpacked[10])
        checksum = int(unpacked[11])
        bad_cksum = 0
        if checksum != sum(unpacked[:-1]):
            bad_cksum = 1
        in_waiting = self._port.inWaiting()
        return (yaw, pitch, roll,
                yaw_rate, pitch_rate, roll_rate,
                bad_cksum, ticks, in_waiting)
        

# Wrapper around the quadsim (simulated plant) that impersonates an IMU.
# (Note to self: this may be overdoing it since the base IMU interface
# only has one method)
class PassthruIMU(IMU):
    def __init__(self, quadsim):
        IMU.__init__(self)
        self.quadsim = quadsim
        
    def ypr(self):
        return self.quadsim.ypr()
        
class PIDParamSet:
    def __init__(self, pid_tuple):
        self.P = pid_tuple[0]
        self.I = pid_tuple[1]
        self.D = pid_tuple[2]

class SineParamSet:
    def __init__(self, frequency_amplitude_tuple):
        self.frequency = frequency_amplitude_tuple[0]
        self.amplitude = frequency_amplitude_tuple[1]
        
# A Decorator that injects a sine wave that rides on the throttle signal
# of the airframe_state
class SignalInjector:
    def __init__(self, frequency, amplitude ):
        self.w = 6.28 * frequency   # angular frequency
        self.a = amplitude          # amplitude
        self.t = 0.0                # current time index
        
        # time step between samples
        self.dt = 1.0 / quadconfig.CONTROL_LOOP_RATE_HZ
                
    def inject(self, airframe_state):
        #################################################
        # need to update airframe_state keys, so stop here
        #
        raise NotImplementedError
        ###############################
        
        airframe_state["y"] = airframe_state["throttle_roll_offset"]
        airframe_state["u_a"] = self._get_next_sample()
        airframe_state["u_r"] = airframe_state["y"] + airframe_state["u_a"]
        
        # if we're not injecting (i.e. when amplitude == 0.0), this
        # function should have no effect on throttle_roll_offset
        airframe_state["throttle_roll_offset"] = airframe_state["u_r"]
        
    def _get_next_sample(self):
        y = sin(self.w * self.t) * self.a
        self.t += self.dt
        return y

class Compensator:
    def __init__(self):
        self.injector = SignalInjector(0.0, 0.0)
        
    def update(self, airframe_state):
        airframe_state["throttle_collective"] = \
            airframe_state["command_collective"]

        # error
        e_yaw = airframe_state["command_yaw"] - airframe_state["yaw_rate"]
        e_pitch = airframe_state["command_pitch"] - airframe_state["pitch"]
        e_roll = airframe_state["command_roll"] - airframe_state["roll"]
        
        # P
        p_pitch = e_pitch * quadconfig.K_p
        p_roll = e_roll * quadconfig.K_p
                
        # D
        d_yaw = -airframe_state["yaw_rate"] * quadconfig.K_d_yaw
        d_pitch = -airframe_state["pitch_rate"] * quadconfig.K_d
        d_roll = -airframe_state["roll_rate"] * quadconfig.K_d

        airframe_state["compensator_e_yaw"] = e_yaw
        airframe_state["compensator_d_yaw"] = d_yaw
        airframe_state["compensator_e_pitch"] = e_pitch
        airframe_state["compensator_p_pitch"] = p_pitch
        airframe_state["compensator_d_pitch"] = d_pitch        
        airframe_state["compensator_e_roll"] = e_roll
        airframe_state["compensator_p_roll"] = p_roll
        airframe_state["compensator_d_roll"] = d_roll
        
        # safety: collective of zero means "do nothing"
        if airframe_state["throttle_collective"] == 0.0:
            airframe_state["throttle_yaw_offset"] = 0.0
            airframe_state["throttle_pitch_offset"] = 0.0
            airframe_state["throttle_roll_offset"] = 0.0
        else:
            airframe_state["throttle_roll_offset"] = p_roll + d_roll
            airframe_state["throttle_pitch_offset"] = p_pitch + d_pitch
            airframe_state["throttle_yaw_offset"] = e_yaw + d_yaw
                
    def set_pid_params(self, pid_params):
        print "Compensator.set_pid_params() called..."
        print "params: ", pid_params.P, pid_params.D
        quadconfig.K_p = pid_params.P
        quadconfig.K_d = pid_params.D
        
    def inject_sinewave(self, sine_params):
        self.injector = SignalInjector(sine_params.frequency,
                                       sine_params.amplitude)

class UDPAirframeStateBroadcaster(threading.Thread):
    def __init__(self):
        # UDP broadcast socket for state broadcasts
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.packet_queue = deque()
        self.quit = False
        threading.Thread.__init__(self)
        
    def run(self):
        while(self.quit == False):
            # broadcast
            while(len(self.packet_queue) > 0 and (self.sock != None)):
                state = self.packet_queue.popleft()
                s = state.to_string()
                self.sock.sendto(s, (LOG_BROADCAST_ADDR, LOG_BROADCAST_PORT))
                time.sleep(0.01)
            time.sleep(0.01)
        self.sock.close()
        
    def send(self, s):
        self.packet_queue.append(s)
        
def get_filtered_cmd(cmd_queue):
    if len(cmd_queue) < CMD_QUEUE_LEN:
        return ControlCommand()
    
    yaw_history = []
    pitch_history = []
    roll_history = []
    collective_history = []
    for i in range(CMD_QUEUE_LEN):
        yaw_history.append(cmd_queue[i].yaw)
        pitch_history.append(cmd_queue[i].pitch)
        roll_history.append(cmd_queue[i].roll)
        collective_history.append(cmd_queue[i].collective)

    # convert to numpy array so we can do some math
    yaw_history = np.array(yaw_history)
    pitch_history = np.array(pitch_history)
    roll_history = np.array(roll_history)
    collective_history = np.array(collective_history)
    
    cmd = ControlCommand()
    cmd.yaw = np.median(yaw_history)
    cmd.pitch = np.median(pitch_history)
    cmd.roll = np.median(roll_history)
    cmd.collective = np.median(collective_history)
            
    return cmd
    
def main():
    
    start_time = time.time()
    
    # we have needs
#    os.nice(-10)

    # open a serial port on the robocontroller
    try:
        p = serial.Serial('/dev/ttyACM0', 115200, timeout=0.001)
    except serial.SerialException:
        print "/dev/ttyACM0 not available...no point in continuing so exiting..."
        sys.exit(-1)
    p.flushInput()
        
    # initialize the command queue
    cmd_queue = deque(maxlen=CMD_QUEUE_LEN)

    broadcaster = UDPAirframeStateBroadcaster()
    broadcaster.start()
    sleeper = sleep.Sleeper()
    airframe_state = AirframeState()
    
    cmd_server = CommandServer(
        quadconfig.CONTROL_PORT, quadconfig.CONTROL_MSG_LEN)
    cfg_server = CommandServer(
        quadconfig.CONFIG_PORT, quadconfig.CONFIG_MSG_LEN)
    cmd_server.start()
    cfg_server.start()
    
    e_stop = False

    loop_count = 0
    read_count = 0
    state = AirframeState()
    robopacket_len = struct.calcsize(AirframeState().format)
    try:
        while(1):
            data = p.read(robopacket_len)
            if len(data) == robopacket_len:
                read_count += 1
                unpacked = struct.unpack(AirframeState().format, data)
                state["elapsed"] = unpacked[0]
                state["yaw"] = unpacked[1]
                state["pitch"] = unpacked[2]
                state["roll"] = unpacked[3]
                state["yaw_rate"] = unpacked[4]
                state["pitch_rate"] = unpacked[5]				
                state["roll_rate"] = unpacked[6]
                state["throttle_collective"] = unpacked[7]
                state["throttle_yaw_offset"] = unpacked[8]
                state["throttle_pitch_offset"] = unpacked[9]
                state["throttle_roll_offset"] = unpacked[10]	
                state["command_yaw"] = unpacked[11]
                state["command_pitch"] = unpacked[12]
                state["command_roll"] = unpacked[13]				
                state["command_collective"] = unpacked[14]
                state["command_heading"] = unpacked[15]
                state["compensator_e_yaw"] = unpacked[16]
                state["compensator_p_yaw"] = unpacked[17]
                state["compensator_d_yaw"] = unpacked[18]
                state["compensator_e_pitch"] = unpacked[19]
                state["compensator_p_pitch"] = unpacked[20]
                state["compensator_d_pitch"] = unpacked[21]
                state["compensator_e_roll"] = unpacked[22]
                state["compensator_p_roll"] = unpacked[23]
                state["compensator_d_roll"] = unpacked[24]
                state["compensator_K_p"] = unpacked[25]
                state["compensator_K_d"] = unpacked[26]
                state["compensator_K_p_yaw"] = unpacked[27]
                state["compensator_K_d_yaw"] = unpacked[28]                
                state["imu_bad_checksums"] = unpacked[29]
                state["imu_ticks"] = unpacked[30]
                state["imu_samples_in_buffer"] = unpacked[31]
                broadcaster.send(state)
                
            # get control and config commands;
            # pass them to robocontroller
            if(cmd_server.in_waiting() > 0):
                cmd = ControlCommand()
                control_str = cmd_server.get_command()
                control_str = cmd.to_pulse_width_msg(control_str)
                p.write(control_str)

            if(cfg_server.in_waiting() > 0):
                cmd = ControlCommand()
                config_str = cfg_server.get_command()
                config_msg = cmd.to_config_msg(config_str)
                p.write(config_msg)
          
    except KeyboardInterrupt:
        if None != p:
            p.close()
        
        print "Waiting for Control CommandServer to quit..."
        cmd_server.quit = True
        cmd_server.join()

        print "Waiting for Configuration CommandServer to quit..."
        cfg_server.quit = True
        cfg_server.join()
        
        print "Waiting for UDP broadcaster to quit..."
        broadcaster.quit = True
        broadcaster.join()
        
        print "HL_controller exiting..."

# main controller loop
if __name__ == "__main__":
    if (len(sys.argv) == 2) and (sys.argv[1] == 'profile'):
        cProfile.run('main()', 'hl_controller_profile')
    else:
        main()
        


























