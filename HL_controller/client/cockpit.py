#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from cPickle import dumps, loads
import pdb
import sys
import time

import pygame
from pygame.locals import *
pygame.init()

from common import quadconfig
from common.control_command import ControlCommand
from common.sleep import Sleeper
from HL_controller.client.controller_client import QuadObserver

import dial

text_labels = [
    "throttle_collective",
    "throttle_yaw_offset",
    "throttle_pitch_offset",
    "throttle_roll_offset",
    "command_yaw",
    "command_pitch",
    "command_roll",
    "command_collective",
    "command_heading",
    "yaw",
    "compensator_e_yaw",
    "compensator_K_p_yaw",
    "compensator_K_d_yaw",
    "compensator_K_p",
    "compensator_K_d",
    "imu_ticks",
    "elapsed"
]

THROTTLE_MIN = 1100
THROTTLE_MAX = 2000
THROTTLE_RANGE = THROTTLE_MAX - THROTTLE_MIN

class Throttles(object):
    """
    GUI representation of throttle values.
    """
    def __init__(self, x=0,  y=0, w=0, h=0, bg_color=0):
        self.surf = pygame.Surface((w, h)).convert()
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.bg_color = bg_color
        self.rect = self.surf.get_rect()
        self.rect = self.rect.move(x, y)
        
        # motor positions, in clockwise order, where 0 is the forward motor
        self.rmax = self.w / 4
        self.rmin = self.rmax / 2
        self.motor_positions = [(self.w / 2, self.rmax), (3 * self.rmax, self.h / 2),
            (self.w / 2, 3 * self.rmax), (self.rmax, self.h / 2)]
        
    def update(self, screen, throttle_vals):
        rmax = self.rmax
        rmin = self.rmin
        self.surf.fill(self.bg_color)
        default_color = pygame.Color("white")
        
        for i in range(4):
            throttle_percent = (throttle_vals[i] - THROTTLE_MIN) / float(THROTTLE_RANGE)
            
            # set circle color
            color = default_color
            if throttle_vals[i] <= THROTTLE_MIN:
                color = pygame.Color("green")
            if throttle_vals[i] >= THROTTLE_MAX:
                color = pygame.Color("red")
                throttle_vals[i] = THROTTLE_MAX
            
            r = int(rmin + (rmax - rmin) * throttle_percent)
            if r < 5:
                r = 5
            pygame.draw.circle(self.surf,
                                color,
                                self.motor_positions[i],
                                r,
                                5)
            
        screen.blit(self.surf, (self.x, self.y))
                         
def render_text(screen, airframe_state):
    """Draw text labels and values on separate lines in a single column,
    return coord of bottom edge of last line drawn."""
    font = pygame.font.Font(None, 24)
    h = font.get_linesize()
    ypos = 0
    for label in text_labels:
        val = str(airframe_state[label])
        s = label + ": " + val
        text = font.render(s, 1, (255, 255, 255))
        screen.blit(text, (0, ypos))
        ypos += h
    return ypos + h
    
def main():
    sleeper = Sleeper()
    qo = QuadObserver(10)
    qo.daemon = True
    qo.start()

    # Initialise screen and sound.
    screen = pygame.display.set_mode((1024, 768))
    screen.fill(0x222222)
    screen_rect = screen.get_rect()
    horizon = dial.Horizon(600, 0)
    heading = dial.DialWithHeadingBug(470, 180, 200, 200) 
    throttles = Throttles(screen_rect.centerx, 0, screen_rect.width / 2, screen_rect.width / 2)
    pygame.mixer.music.load('beep.wav')
    
    loop_count = 0
    log = []
    marks = []
    tick = 0.0
    g_quit = False
    while(not g_quit):
        for event in pygame.event.get():
            if event.type == QUIT:
               print "Caught Pygame.QUIT...exiting"
               sys.exit()   # end program.
            
            if event.type == KEYDOWN:
                if event.key == K_SPACE:
                    pygame.mixer.music.play(0)
                    marks.append(tick)
               
        try:
            screen.fill(0x222222) # erases the entire screen surface
            history = qo.get_all()
            if len(history) == 0:
                print "len(history) == 0"
                time.sleep(0.5)
                continue
            state = history[-1]
            tick = state["imu_ticks"]
            roll = state["roll"]
            pitch = state["pitch"]
            y = render_text(screen, state)
            horizon.position(0, y)
            horizon.update(screen, roll, pitch)
            heading.position(horizon.dial.get_rect().right, horizon.y)
            heading.update(screen, state["yaw"], state["command_heading"])
            tc = state["throttle_collective"]
            tyo = state["throttle_yaw_offset"]
            tpo = state["throttle_pitch_offset"]
            tro = state["throttle_roll_offset"]
            t0 = tc - tyo + tpo 
            t1 = tc + tyo - tro
            t2 = tc - tyo - tpo
            t3 = tc + tyo + tro
            throttles.update(screen, [t0, t1, t2, t3])
            pygame.display.update()
            log += history
            time.sleep(0.1)
            
        except KeyboardInterrupt:
            print "Caught KeyboardInterrupt...exiting and writing log."
            qo.quit = True
            
            # open a log file and a mark file
            t = time.localtime()
            logfilename = '%4d' % t.tm_year + \
                          '%02d' % t.tm_mon + \
                          '%02d' % t.tm_mday + \
                          '_' + \
                          '%02d' % t.tm_hour + \
                          '%02d' % t.tm_min + \
                          '_cockpit_log' + \
                          '.txt'
            
            markfilename = '%4d' % t.tm_year + \
                          '%02d' % t.tm_mon + \
                          '%02d' % t.tm_mday + \
                          '_' + \
                          '%02d' % t.tm_hour + \
                          '%02d' % t.tm_min + \
                          '_markfile_log' + \
                          '.txt'
                                    
            logfile = open(logfilename, 'wb')
            markfile = open(markfilename, 'wb')
            
            # write the log header
            states = log[0].keys()
            states.remove("imu_ticks")
            states.insert(0, "imu_ticks")
            hdr = ""
            for state in states:
                hdr += state + ","
            hdr += '\n'
            logfile.write(hdr)
            
            # write the data
            for entry in log:
                logstring = ""
                for state in states:
                    logstring += str(entry[state]) + ","
                logstring += '\n'
                logfile.write(logstring)
            logfile.close()
            
            # write markfile
            for mark in marks:
                markfile.write(str(mark) + '\n')
            markfile.close()
            
            g_quit = True
            
        except Exception as e:
            pdb.set_trace()
            pass

if __name__ == "__main__":
    main()
