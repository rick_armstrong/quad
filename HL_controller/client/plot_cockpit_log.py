#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import pdb
from pdb import set_trace
#import win32ui
import Tkinter, Tkconstants, tkFileDialog

def main():
#    dlg = win32ui.CreateFileDialog(True, "", "", 0, "")
#    dlg.SetOFNTitle("Select cockpit_log file")
#    dlg.DoModal()
#    logfile_path = dlg.GetPathName()
    opt = {'initialdir' : os.getcwd()}
    logfile_path = tkFileDialog.askopenfilename(**opt)    

    # get column indices
    f = open(logfile_path, 'r')
    first_line = f.readline()
    f.close()
    columns = first_line.split(",")
    
    ### EDIT HERE: enter aircraft states you'd like to plot.
    ### Time index is assumed to be the first column in the data
    columns_to_plot = [
        "imu_ticks",
        "throttle_collective",
        "command_heading",
        "command_yaw",
        "yaw",
#        "pitch",
        "roll",
        "yaw_rate",
#        "compensator_d_roll",
        "compensator_e_yaw",
        "throttle_yaw_offset",
    ]
    
    column_indices = []
    for col in columns_to_plot:
        column_indices.append(columns.index(col))    
    data = []
    data = np.loadtxt(
        logfile_path, delimiter=',', skiprows=1,
                           usecols=column_indices,
                           unpack=True)

#    print "roll stdev: ", data[1].std()
    #difference = list(np.diff(data[1]))
    #difference = [0] + difference
    #data[1] = np.array(difference)
    
    # break out the time data separately
    data = list(data) 
    ticks = data.pop(0)
    columns_to_plot.pop(0)    
    
    for i in range(len(data)):
        plt.plot(ticks, data[i], label=columns_to_plot[i])
    plt.legend()
    plt.show()
    
if __name__ == "__main__":
    main()
