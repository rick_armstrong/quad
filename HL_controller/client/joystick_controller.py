#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import pdb
import pygame
from pygame.locals import *
import sys
import threading

from HL_controller.client import controller_client
from common import quadconfig
from common.sleep import Sleeper

MAX_TILT_COMMAND_DEGREES = 10.0
MAX_YAW_COMMAND_THROTTLE = 100.0
YAW_AUTHORITY = 1.0
YAW_DEADBAND = 25.0

axes = {'yaw' : 0, 'pitch' : 0, 'roll' : 0, 'collective' : 0}
axes_lock = threading.Lock()

axis_names = ['roll', 'pitch', 'collective', 'yaw']

def update_yaw(joy_value, axes):
    out = joy_value * MAX_YAW_COMMAND_THROTTLE
    
    # add some deadband
    if abs(out) > YAW_DEADBAND:
        axes['yaw'] = joy_value * MAX_YAW_COMMAND_THROTTLE
    else:
        axes['yaw'] = 0.0
    
def update_pitch(joy_value, axes):
    axes['pitch'] = joy_value * MAX_TILT_COMMAND_DEGREES

def update_roll(joy_value, axes):
    axes['roll'] = joy_value * MAX_TILT_COMMAND_DEGREES

def update_collective(joy_value, axes):
    value = ((joy_value + 1.0) / 2.0) * 255.0
    axes['collective'] = value

axis_updaters = {'yaw' : update_yaw, 'pitch' : update_pitch,
                 'roll' : update_roll, 'collective' : update_collective}

g_quit = False

class JoystickMonitor(threading.Thread):
    def __init__(self, joystick, axis_updaters, axis_names, axes_lock):
        threading.Thread.__init__(self)
        self.joy = joystick        
        self.axis_updaters = axis_updaters
        self.axis_names = axis_names
        self.axes_lock = axes_lock
    
    def run(self):
        global g_quit
        while(not g_quit):
            pygame.event.pump()
            yaw = -joy.get_axis(axis_names.index('yaw'))
            pitch = joy.get_axis(axis_names.index('pitch'))
            roll = joy.get_axis(axis_names.index('roll'))
            collective = joy.get_axis(axis_names.index('collective'))
            self.axes_lock.acquire()
            self.axis_updaters['yaw'](yaw, axes)
            self.axis_updaters['pitch'](pitch, axes)
            self.axis_updaters['roll'](roll, axes)            
            self.axis_updaters['collective'](collective, axes)
            self.axes_lock.release()
            sleeper.sleep(0.1 * 1e6)            

class ControlMessageSender(threading.Thread):
    def __init__(self, quad_controller, axes, axes_lock):
        threading.Thread.__init__(self)
        self.qc = quad_controller
        self.axes = axes
        self.axes_lock = axes_lock
        
    def run(self):
        global g_quit
        while(not g_quit):
            self.axes_lock.acquire()
            self.qc.send_control_msg(axes['yaw'], axes['pitch'], axes['roll'],
                                axes['collective'])
            
            print("YPRC:", axes['yaw'], axes['pitch'], axes['roll'],
                axes['collective'])
            self.axes_lock.release()
            sleeper.sleep(0.1 * 1e6)
        
        print "ControlMessageSender exiting..."
        self.qc.disconnect()
        return
    
if __name__ == "__main__":
    sleeper = Sleeper()

    # init pygame
    pygame.init()
    pygame.joystick.init()
    joy = pygame.joystick.Joystick(0)
    joy.init()

    # connect a client
    qc = controller_client.QuadClient('192.168.4.4', 4444, 4445)
    qc.connect()
    qc.send_control_msg(0.0, 0.0, 0.0, 0.0)
    print 'QuadClient qc connected and sent throttles off command'
    
    jeh = JoystickMonitor(joy, axis_updaters, axis_names, axes_lock)
    cms = ControlMessageSender(qc, axes, axes_lock)
   
    jeh.start()
    cms.start()
    while(not g_quit):
        try:
            sleeper.sleep(0.1 * 1e6)
        except KeyboardInterrupt:
            print "Caught Ctrl-c, signaling threads to exit..."
            g_quit = True
            cms.join()
            sys.exit()

    
    #while(1):
    #    for evt in pygame.event.get():
    #        if evt.type == pygame.locals.JOYAXISMOTION:
    #            axis_name = axis_names[evt.axis]
    #            axis_updaters[axis_name](evt.value, axes)
    #
    #        if evt.type == pygame.locals.QUIT:
    #            qc.disconnect()
    #            sys.exit()
                
        #qc.send_control_msg(axes['yaw'], axes['pitch'], axes['roll'],
        #                    axes['collective'])
        #print("YPRC:", axes['yaw'], axes['pitch'], axes['roll'],
        #    axes['collective'])
        #sleeper.sleep(0.1 * 1e6)
