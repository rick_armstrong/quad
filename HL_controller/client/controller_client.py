#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from __future__ import with_statement
from collections import deque
import cPickle
import socket
from struct import pack
from struct import Struct
import threading
import time
import pdb
from pdb import set_trace

from common.quadconfig import CONTROL_LOOP_RATE_HZ
from common.quadconfig import LOG_BROADCAST_LISTEN_IF, LOG_BROADCAST_ADDR
from common.quadconfig import LOG_BROADCAST_PORT
from common.quadconfig import CONFIG_MSG_LEN
from common.ring_buffer import RingBuffer
from HL_controller.hl_controller import CFG_MGSID_PID_PARAMS
from HL_controller.hl_controller import PIDParamSet
from HL_controller.hl_controller import CFG_MSGID_INJECT_SINE
from HL_controller.hl_controller import SineParamSet
from common.airframe_state import AirframeState

QUAD_CONTROL_HOST = 'localhost'

class QuadObserver(threading.Thread):
    def __init__(self, record_len_s):
        self.quit = False
        self.max_samples = CONTROL_LOOP_RATE_HZ * record_len_s
        self.airframe_state_queue = deque(maxlen=self.max_samples)
        print "QuadObserver: keeping ", self.max_samples, " samples in buffer"
        self.port = LOG_BROADCAST_PORT
        self.sock = None
        print ("QuadObserver: binding new listener socket on port: ",
               LOG_BROADCAST_PORT)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)        
        self.sock.bind((LOG_BROADCAST_ADDR, self.port))        
        threading.Thread.__init__(self)
        
    def run(self):
        """Poll the UDP listener socket and push samples into the buffer."""
        while(self.quit == False):
            try:
                # buffersize is suggested in documentation for recvfrom
                (s, addr) = self.sock.recvfrom(4096)
                state = AirframeState.from_string(s)
                self.airframe_state_queue.append(state)
                
            except Exception, e:
                print e
                break

        print "QuadObserver: closing listener socket."
        self.sock.close()
        self.sock = None
 
    def get(self):
        """Remove and return the oldest sample in the buffer,
        or ObserverEmtpyError if there are no samples to get."""
        try:
            state = self.airframe_state_queue.popleft()
            return state
        except IndexError:
            raise ObserverEmptyError()
    
    def get_all(self):
        """Remove and return a list of all samples currently in the buffer."""
        n = len(self.airframe_state_queue)
        tmp = []
        while(n > 0):
            state = self.airframe_state_queue.popleft()
            tmp.append(state)
            n -= 1
        return tmp
    
    def count(self):
        """Return the number of samples in the buffer received from the Quad."""
        return len(self.airframe_state_queue)

class ObserverEmptyError(Exception):
    """Raised when a user attempts to get samples from an empty QuadObserver."""
    def __init__(self):
        pass
        
class QuadClient:
    def __init__(self, host, ctrlPort, cfgPort):
        self.host = host
        self.ctrlPort = ctrlPort
        self.cfgPort = cfgPort
        self.ctrlSock = None
        self.cfgSock = None
        
    def connect(self):
        
        # socket for control channel
        if self.ctrlSock == None:
            self.ctrlSock = socket.socket(
                        socket.AF_INET,
                        socket.SOCK_STREAM)            
            self.ctrlSock.connect((self.host, self.ctrlPort))
        
        # socket for config channel    
        if self.cfgSock == None:
            self.cfgSock = socket.socket(
                        socket.AF_INET,
                        socket.SOCK_STREAM)            
            self.cfgSock.connect((self.host, self.cfgPort))
            

    def disconnect(self):
        self.ctrlSock.shutdown(socket.SHUT_RDWR)
        self.ctrlSock.close()
        self.ctrlSock = None
        self.cfgSock.shutdown(socket.SHUT_RDWR)
        self.cfgSock.close()
        self.cfgSock = None
        
    def send_control_msg(self, yaw, pitch, roll, collective):
        msg = pack('ffff', yaw, pitch, roll, collective)
        if self.ctrlSock != None:
            self.ctrlSock.send(msg)
        else:
            print "Not connected"
            
    def _send_config_msg(self, msg):
        if self.cfgSock != None:
            self.cfgSock.send(msg)
        else:
            print "Not connected"
        
    def set_PID(self, K_p, K_i, K_d, K_d_yaw):
        """Send a PID config parameter message"""
        msg = pack('ffff', K_p, K_i, K_d, K_d_yaw)
        self._send_config_msg(msg)
        
    def inject_sinewave(self, sine_param_tuple):
        params = SineParamSet(sine_param_tuple)
        params_pickled = cPickle.dumps(params)
        cfg_packet = pack('B', CFG_MSGID_INJECT_SINE) # id
        cfg_packet += pack('B', len(params_pickled))   # data len
        cfg_packet += params_pickled # data
        pad = CONFIG_MSG_LEN - len(params_pickled) - 2
        cfg_packet += pack(str(pad) + 'x') # pad to cfg packet len
        self.send_config_msg(cfg_packet)

# tests
def test_run_until_buffer_full():
    qo = QuadObserver(5)
    qo.start()

    while(True):
        n = qo.count()
        print "record count: ", n
        if qo.count() >= qo.max_samples:
            print "QuadObserver buffer full, waiting for thread shutdown..."
            qo.quit = True
            qo.join()
            break
        time.sleep(1)
    print "*** PASS ***"

def test_get_all():
    """Verify that get_all() clears the buffer and that the buffer.
    We create an Observer with a three-second buffer, and empty it
    once per second. We should never fill the buffer."""
        
    qo = QuadObserver(3)
    qo.start()
    for i in range(3):
        tmp = qo.get_all()
        print "Pulled ", len(tmp), " samples from the Observer."
        print "Observer now has ", qo.count(), " samples in the buffer."
        time.sleep(2.5)

    print "Stopping QuadObserver..."
    qo.quit = True
    qo.join()
    print "*** PASS ***"    
        
def test_get_on_empty_observer():
    qo = QuadObserver(1)
    passed = False
    try:
        qo.get()
    except Exception, e:
        if type(e) == ObserverEmptyError:
            passed = True
            print "Caught ObserverEmptyError."
        
    assert(passed)
    print "*** PASS ***"
        
if __name__ == "__main__":
    print ""
    print "***"
    print "*** TEST: run observer until full then stop: ***"
    print "***"
    test_run_until_buffer_full()
    
    print ""
    print "***"
    print "*** TEST: verify that get_all() clears buffer: ***"
    print "***"
    test_get_all()

    print ""
    print "***"
    print "*** TEST: verify that empty QuadObserver throws ObserverEmptyError:"
    print "***"
    test_get_on_empty_observer()
    
    print ""
    print "All tests passed."































    