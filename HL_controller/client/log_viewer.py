# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""This script loads two files, interactively selected by the user:
1) a log file produced by cockpit.py.
2) a video of the corresponding flight.

Before running this script, it is necessary to edit the section below marked
'EDIT THIS', to set the correspondence between timestamps in the video and
in the log file. The best way to do this is to use the 'markfile' feature of
cockpit.py: point the video camera at the PC running cockpit.py, and hit the
spacebar. cockpit.py will output a number representing the log entry index at
the moment that you hit the spacebar. Find the corresponding video time in the
video, and enter them both below.
Note: most of this code is taken directly from the examples in vlc.py.
"""
import numpy as np
import os
import pdb
import sys
import time
from vlc import EventType
from vlc import Instance
import win32ui

from common.airframe_state import AirframeState
from HL_controller.hl_controller import UDPAirframeStateBroadcaster

#----------------EDIT THIS------------------
# video time (milliseconds), elapsed from logfile (milliseconds)
zero_time = {'video': 3402.0, 'logfile': 727993.134} 

# time interval between MediaPlayerTimeChanged events
VIDEO_TICK_INTERVAL_ms = 300

def lookup_aircraft_state(video_time_ms, log_entries, log_time_indices_ms):
    """Find the log entry that corresponds to the video_time and return it."""
    # convert video_time to imu_ticks
    video_time_relative_ms = video_time_ms - zero_time['video']
    log_time_abs_ms = zero_time['logfile'] + video_time_relative_ms
    
    # find the nearest corresponding log entry
    less = filter(lambda x : x < log_time_abs_ms, log_time_indices_ms)
    log_idx = len(less)
    line = log_entries[log_idx]
    return line

def parse_log_entry(log_entry, columns, airframe_state):
    """Parse a single log entry (string) using the passed-in column names,
    then populate the passed-in AirframeState"""
    state_vars = log_entry.split(",")
    for state in airframe_state.states:
        t = type(airframe_state[state])
        airframe_state[state] = t(state_vars[columns.index(state)])

if __name__ == '__main__':
    
    # initialize a UDP broadcaster
    broadcaster = UDPAirframeStateBroadcaster()
    broadcaster.start()

    try:
        from msvcrt import getch
    except ImportError:
        import termios
        import tty

        def getch():  # getchar(), getc(stdin)  #PYCHOK flake
            fd = sys.stdin.fileno()
            old = termios.tcgetattr(fd)
            try:
                tty.setraw(fd)
                ch = sys.stdin.read(1)
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, old)
            return ch
    
    # ask the user where our files are
    dlg = win32ui.CreateFileDialog(True, "", "", 0, "")
    dlg.SetOFNTitle("Select a log file from cockpit.py")
    dlg.DoModal()
    logfile_path = dlg.GetPathName()
    dlg.SetOFNTitle("Select the corresponding video file")    
    dlg.DoModal()
    videofile_path = dlg.GetPathName()

    # get column indices from the log file, and read the whole thing in
    f = open(logfile_path, 'r')
    first_line = f.readline()
    lines = f.readlines()
    f.close()
    columns = first_line.split(",")
    
    # extract the elapsed column
    time_indices = np.loadtxt(logfile_path, delimiter=',', skiprows=1, \
                   usecols=(columns.index("elapsed"),), unpack=True)
    
    # the 'elapsed' entries int the log are in microseconds, convert to ms
    time_indices_ms = time_indices / 1000.0
    
    video = os.path.expanduser(videofile_path)
    if not os.access(video, os.R_OK):
        print('Error: %s file not readable' % video)
        sys.exit(1)

    instance = Instance("--sub-source marq")
    try:
        media = instance.media_new(video)
    except NameError:
        print('NameError: %s (%s vs LibVLC %s)' % (sys.exc_info()[1],
                                                   __version__,
                                                   libvlc_get_version()))
        sys.exit(0)
    player = instance.media_player_new()
    player.set_media(media)
    player.play()
    
    state = AirframeState()
    def time_changed_callback(event):
        '''Use the incoming time index to look up the corresponding log entry,
        parse the log entry, and broadcast it via UDP.
        '''
        print event.u.new_time
        
        # don't do anything if we haven't reached zero time yet
        if event.u.new_time < zero_time['video']:
            return
        log_entry = lookup_aircraft_state(event.u.new_time, lines, \
                                          time_indices_ms)
        parse_log_entry(log_entry, columns, state)
        broadcaster.send(state)

    event_manager = player.event_manager()
    event_manager.event_attach(EventType.MediaPlayerTimeChanged, \
                               time_changed_callback)

    def seconds_forward():
        player.set_time(player.get_time() + 1000)
    
    def seconds_backward():
        player.set_time(player.get_time() - 1000)
    
    def pause():
        player.pause()
        
    def die():
        print "Waiting for UDP broadcaster to quit..."
        broadcaster.quit = True
        broadcaster.join()
        
        print "Releasing player and exiting."
        player.release()
        sys.exit(0)

    keybindings = {
        ' ': pause,
        'q': die,
        '+': seconds_forward,
        '-': seconds_backward
        }
        
    while True:
        key = getch()
        if key in keybindings:
            keybindings[key]()
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    