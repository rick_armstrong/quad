#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from math import sin
from math import cos
import numpy as np
import serial
import time
from common import quadconfig
from common import throttleControl

### Simulated PowerPlant + IMU
class QuadSim:
    def __init__(self):
        pass
    
    def set_throttle(self,
                    yaw_offset,
                    pitch_offset,
                    roll_offset,
                    collective):
        pass
    
    def ypr(self):
        yaw = sin(time.clock())
        pitch = cos(time.clock())
        roll = sin(time.clock())
        return (yaw, pitch, roll, 0.0)
        
### This is the real (not simulated) interface to the quad's ESCs
class PowerPlant:
    def __init__(self):

        self.loop_count = 0
        
        # converts a vector of throttle commands (collective, roll_offset,
        # pitch_offset, yaw_offset) to rotor speeds.
        self.A_throttle_to_w = np.array([
            (1, 0, 1, -1),
            (1, -1, 0, 1),
            (1, 0, -1, -1),
            (1, 1, 0, 1)])

         # motor speeds
        self.w_i = np.dot(self.A_throttle_to_w, np.array([0.0, 0.0, 0.0, 0.0]).T)
 
        self.port = serial.Serial(
            quadconfig.LL_CONTROLLER_PORT_STR, 
            quadconfig.LL_CONTROLLER_PORT_BAUD,
            timeout=quadconfig.LL_CONTROLLER_PORT_TIMEOUT_s)       
        throttleControl.set_throttle([0, 0, 0, 0], self.port)
          
        # kill all motors
        throttleControl.set_throttle([0, 0, 0, 0], self.port)
        self.port.read(6)

        # enable the IMU by telling the MicroMaestro to pull ch5 low
        self.port.write(chr(0x84) + chr(5) + chr(0) + chr(0))    
    
    def set_throttle(self,
                    yaw_offset,
                    pitch_offset,
                    roll_offset,
                    collective):

        throttles = np.array([collective,
                              roll_offset,
                              pitch_offset,
                              yaw_offset])
        self.w_i = np.dot(self.A_throttle_to_w, throttles.T)

        self.loop_count = self.loop_count + 1
        #if((self.loop_count % quadconfig.CONTROL_LOOP_RATE_HZ) == 0):
        #    print(
        #        int(self.clamp(self.w_i[0], 0, 255)), " ",
        #        int(self.clamp(self.w_i[1], 0, 255)), " ",
        #        int(self.clamp(self.w_i[2], 0, 255)), " ",
        #        int(self.clamp(self.w_i[3], 0, 255))
        #        )

        throttleControl.set_throttle(
            [int(self.clamp(self.w_i[0], 0, 255)),
             int(self.clamp(self.w_i[1], 0, 255)),
             int(self.clamp(self.w_i[2], 0, 255)),
             int(self.clamp(self.w_i[3], 0, 255))],
            self.port)            
    
    def clamp(self, x, min, max):
        if x < min: 
            x = min
            return x
        if x > max:
            x = max
            return x
        return x

