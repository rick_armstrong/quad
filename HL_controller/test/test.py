#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import time
from common import sleep
import win32api,win32process,win32con
from HL_controller.hl_controller import CommandServer
from common.quadconfig import CONTROL_PORT
from HL_controller.client.controller_client import QuadClient
from HL_controller.client.controller_client import QUAD_CONTROL_HOST
from HL_controller.client.controller_client import QuadObserver


if __name__ == "__main__":
    
    qo = QuadObserver()
    qo.start()
    
    #sleeper = sleep.Sleeper()
    #client = QuadClient(QUAD_CONTROL_HOST, CONTROL_PORT)
    #server = CommandServer(CONTROL_PORT)
    #server.start()
    #sleeper.sleep(1e6)
    #client.connect()
    #client.send_control_msg(0, 0, 0, 0)
    
    #pid = None
    #priorityclasses = [win32process.IDLE_PRIORITY_CLASS,
    #                   win32process.BELOW_NORMAL_PRIORITY_CLASS,
    #                   win32process.NORMAL_PRIORITY_CLASS,
    #                   win32process.ABOVE_NORMAL_PRIORITY_CLASS,
    #                   win32process.HIGH_PRIORITY_CLASS,
    #                   win32process.REALTIME_PRIORITY_CLASS]
    #if pid == None:
    #    pid = win32api.GetCurrentProcessId()
    #handle = win32api.OpenProcess(win32con.PROCESS_ALL_ACCESS, True, pid)
    #win32process.SetPriorityClass(handle, priorityclasses[5])
    #
    #sleeper = sleep.Sleeper()
    #times = [0.0] * 100
    #for i in range(100):
    #    start = time.clock()
    #    #sleeper.sleep(10000)
    #    time.sleep(0.01)
    #    times[i] = time.clock() - start
    #    
    #print 'finished time test'
    #timestrings = []
    #for t in times:
    #    timestrings.append(str(t) + '\n')
    #    
    #f = open('times.csv', 'w')
    #f.writelines(timestrings)
    #f.close()

#def clamp(x, min, max):
#    if x < min: 
#        x = min
#        return x
#    if x > max:
#        x = max
#        return x
#    return x
#
#x = 128
#y = clamp(x, 0, 255)
#
#print 'x = ', x
#print 'y = ', y
#
#x = -300.0
#y = clamp(x, 0, 255)
#
#print 'x = ', x
#print 'y = ', y
#
#x = 300.0
#y = clamp(x, 0, 255)
#
#print 'x = ', x
#print 'y = ', y

#times = [0.0] * 10000
#for i in range(10000):
#    times[i] = time.clock()
#    
#timestrings = []
#for t in times:
#    timestrings.append(str(t) + '\n')
#    
#f = open('times.csv', 'w')
#f.writelines(timestrings)
#f.close()

            
#loop_count = loop_count + 1
#if ((loop_count % CONTROL_LOOP_RATE_HZ) == 0):
#    print (airframe_state.yaw, airframe_state.pitch,
#           airframe_state.roll)
#    winsound.Beep(1000, 10)
        
#>>I have a need for a time.clock() with >0.000016 second (16us) accuracy.
#>>The sleep() (on Python 2.3, Win32, at least) has a .001s limit. Is it lower/better on other's platforms?
#>
#>Try a waitable timer <http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dllproc/base/waitable_timer_objects.asp> 
#>SetWaitableTimer specifies the interval with 100ns granularity, but maybe the actual timer precision depends on hardware or OS version or ...
#
#Thanks!
#Upon searching I found:
#http://mail.python.org/pipermail/python-win32/2003-July/001166.html
#
#from ctypes import *
#import win32event, win32api
#
#h = windll.kernel32.CreateWaitableTimerA(None, 0, None)
##dt = c_longlong(-60L * 10L**(9-2)) ## 60s * 1/(100nanoseconds)
#dt = c_longlong(-160L) ## 16microseconds
#dt_p = pointer(dt)  ## uses 100ns increments
#windll.kernel32.SetWaitableTimer(h, dt_p, 0, None, None, 0)
#win32event.WaitForSingleObject(h, win32event.INFINITE)
#win32api.CloseHandle(h)
