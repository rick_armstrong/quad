#!/usr/bin/env python

# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import time

def frange(start, stop, n):
    L = [0.0] * n
    nm1 = n - 1
    nm1inv = 1.0 / nm1
    for i in range(n):
        L[i] = nm1inv * (start*(nm1 - i) + stop*i)
    return L


#if __name__ == '__main__':
#    ''' time.sleep() on a single value'''
#
#    interval = 0.005
#    n = 10000
#    times = []
#    
#    start = time.time()
#    for i in range(0, n):
#        time.sleep(interval)       
#    end = time.time()
#    total_time = end - start
#    
#    print 'Interval = ' + str(interval) + ' seconds.'
#    print 'Total time for ' + str(n) + ' iterations: ' +  str(total_time)
#    print 'Avg. time: ' + str(total_time / 10000.0)
    
##
## Linux (using time.time() for timing)
##
#if __name__ == '__main__':
#    ''' time.sleep() '''
#    r0 = frange(0.0, 0.01, 10)
#    r1 = frange(0.01, 0.1, 10)
#    r = r0 + r1
#
#    n = 100
#    times = []
#    for interval in r:
#        for i in range(0, n):
#            
#            start = time.time()
#            time.sleep(interval)
#            end = time.time()
#            
#            t = end - start
#            times.append(t)
#            
#    f = open('time.sleep.gumstix.txt', 'w')
#    for t in times:
#        f.write(str(t) + '\n')
#    f.close()




###
### Windows
###
#if __name__ == '__main__':
#    ''' time.sleep() '''
#    r0 = frange(0.0, 0.01, 10)
#    r1 = frange(0.01, 0.1, 10)
#    r = r0 + r1
#
#    n = 100
#    times = []
#    for interval in r:
#        for i in range(0, n):
#            
#            start = time.clock()
#            time.sleep(interval)
#            end = time.clock()
#            
#            t = end - start
#            times.append(t)
#            
#    f = open('time.sleep.windows.txt', 'w')
#    for t in times:
#        f.write(str(t) + '\n')
#    f.close()

#if __name__ == '__main__':
#    ''' common.Sleeper() '''
#    from common import sleep
#    sleeper = sleep.Sleeper()
#    
#    r0 = frange(0.0, 0.01, 10)
#    r1 = frange(0.01, 0.1, 10)
#    r = r0 + r1
#
#    n = 100
#    times = []
#    for interval in r:
#        for i in range(0, n):
#            
#            start = time.clock()
#            sleeper.sleep(interval * 1e6)
#            end = time.clock()
#            
#            t = end - start
#            times.append(t)
#            
#    f = open('Sleeper.sleep.windows.txt', 'w')
#    for t in times:
#        f.write(str(t) + '\n')
#    f.close()