#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import pylab
import numpy
import pdb
from pdb import set_trace
import serial
import struct
from struct import Struct
from common import sleep
from common import quadconfig
from HL_controller.hl_controller import MicrostrainIMU


if __name__ == "__main__":

############ STREAMING MODE ###################################    
    #imu = MicrostrainIMU()
    #try:
    #    y, p, r, ticks = (0, 0, 0, 0)
    #    loop_count = 0
    #    while(1):
    #        y, p, r, ticks = imu.ypr()
    #        loop_count = loop_count + 1
    #        if((loop_count % quadconfig.CONTROL_LOOP_RATE_HZ) == 0):
    #            print "YPR, ticks: ", y, p, r, ticks
    #            
    #except KeyboardInterrupt:
    #    print "Keyboard interrupt received."

            
############ JITTER PLOT ###################################    
    
    imu = MicrostrainIMU()
    timestamps = numpy.zeros(200)
    rolls = numpy.zeros(200)
    for i in range(0, 200):
        y, p, r, ticks = imu.ypr()
        rolls[i] = r
        timestamps[i] = ticks
    
    # one tick is 6.5536 ms
    timediffs = numpy.zeros(200)
    for i in range(1, 200):
        timediffs[i] = timestamps[i] - timestamps[i-1]
    pylab.plot(timediffs)
    pylab.show()
    pdb.set_trace()
