#include <avr/io.h>		// include I/O definitions (port names, pin names, etc)
#include <avr/interrupt.h>	// include interrupt support
#include <string.h>

#include "global.h"		// include our global settings
#include "uart2.h"		// include uart function library
#include "rprintf.h"	// include printf function library
#include "a2d.h"		// include A/D converter function library
#include "timer.h"		// include timer function library (timing, PWM, etc)
#include "servo.h"		// include RC servo driver library
#include "vt100.h"		// include VT100 terminal library

#define MOTOR_COUNT 4

int main(void)
{
	
	// timer
	timerInit();

	// a2d
	a2dInit();
	a2dSetPrescaler(ADC_PRESCALE_DIV32);
	a2dSetReference(ADC_REFERENCE_AVCC);
	DDRF = 0x00;	// set up PORTF as A2D input
	PORTF = 0x00;	// enable pullups 
	
	// servo
	servoInit();	
	servoSetChannelIO(0, _SFR_IO_ADDR(PORTB), PB5);
	servoSetChannelIO(1, _SFR_IO_ADDR(PORTB), PB6);
	servoSetChannelIO(2, _SFR_IO_ADDR(PORTB), PB7);
	servoSetChannelIO(3, _SFR_IO_ADDR(PORTE), PE3);
	
	DDRB = 0xFF; 	// set all PORTB pins to output
	PORTB |= 0x10;	// turn off yellow LED
	DDRE = 0xFF; 	// set all PORTE pins to otuput	

	// start with motors off
	for(u08 motor = 0; motor < MOTOR_COUNT; motor++)
	{
		servoSetPosition(motor, 0);
	}

	while(1)
	{
		u08 throttle = a2dConvert8bit(0);
		for(u08 motor = 0; motor < MOTOR_COUNT; motor++)
		{
			servoSetPosition(motor, throttle);
		}
		timerPause(20); 
	}
	
	return 0;
}