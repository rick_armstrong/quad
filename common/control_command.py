# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import pdb
import struct

from quadconfig import CONTROL_MSG_ID, CONFIG_MSG_ID, MAX_TILT_CMD_DEGREES
class ControlCommand:
    def __init__(self):
        self.yaw = 0.0
        self.pitch = 0.0
        self.roll = 0.0
        self.collective = 0

    def parse_control_string(self, s):
        cmds = struct.unpack('ffff', s)
        self.yaw = self.clamp(cmds[0], 0.0, 0.0)
        self.pitch = self.clamp(cmds[1], -MAX_TILT_CMD_DEGREES, MAX_TILT_CMD_DEGREES)
        self.roll = self.clamp(cmds[2], -MAX_TILT_CMD_DEGREES, MAX_TILT_CMD_DEGREES)
        self.collective = int(cmds[3])

    def clamp(self, x, min, max):
        if x < min: 
            x = min
            return x
        if x > max:
            x = max
            return x
        return x

    def to_pulse_width_msg(self, s):
        self.parse_control_string(s)
        hdr = CONTROL_MSG_ID
        y = 1500 # we leave yaw alone because we don't how to do it right yet
        p = 1500 + (500 * (self.pitch / MAX_TILT_CMD_DEGREES))
        r = 1500 + (500 * (self.roll / MAX_TILT_CMD_DEGREES))
        c = 1000 + (1000 * (self.collective / 255.0))
        out_str = struct.pack('IIIII', hdr, int(p), int(r), int(c), int(y))
        return out_str
    
    def to_config_msg(self, s):
        # The config message is almost good-to-go as-is;
        # we just add the header
        hdr = struct.pack('I', CONFIG_MSG_ID)
        return  hdr + s
        