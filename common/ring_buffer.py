#!/usr/bin/env python
# From http://code.activestate.com/recipes/68429-ring-buffer/
from threading import Lock

class RingBuffer:
    def __init__(self,size_max):
        self.max = size_max
        self.data = []
        self.lock = Lock()
        
    def reset(self):
        self.lock.acquire()
        self.data = []
        self.lock.release()
            
    def append(self,x):
        self.lock.acquire()
        self.data.append(x)
        if len(self.data) == self.max:
            self.cur=0
            self.__class__ = RingBufferFull
        self.lock.release()
        
    def get(self):
        return self.data[:]


class RingBufferFull:
    def __init__(self,n):
            raise "you should use RingBuffer"

    def reset(self):
        self.lock.acquire()
        self.data = []
        self.lock.release()
        self.__class__= RingBuffer
        
    def append(self,x):
        self.lock.acquire()    
        self.data[self.cur]=x
        self.cur=(self.cur+1) % self.max
        self.lock.release()
        
    def get(self):
        self.lock.acquire()
        buf = self.data[self.cur:]+ self.data[:self.cur]
        self.lock.release()
        return buf