# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import copy
import cPickle
import pdb
import struct
import time

from ordered_dict import OrderedDict
from control_command import ControlCommand

class UnknownStateError(Exception):
    """Raised when a user attempts to add or access a new or unknown state.

    Attributes:
    msg -- error string
    state -- the state that the user attempted to add
    """    
    def __init__(self, msg, state):
        self.msg = msg
        self.state = state
        
class AirframeState(object):
    """This class is a (de)serializable dictionary-like container
    for all aircraft states at a given moment.
    Note that the name is slightly misleading since it can contain more than
    airframe states, e.g. compensator innards or anything else we'd like to
    store about the craft.
    This class should be considered immutable and it will raise an
    UnknownStateError if a user attempts to access or add a new or unknown
    state.
    """
    _PICKLED_LEN = 0

    @classmethod
    def pickled_len(cls):
        if AirframeState._PICKLED_LEN == 0:
            s = cPickle.dumps(AirframeState())
            AirframeState._PICKLED_LEN = len(s)      
        return AirframeState._PICKLED_LEN

    def __init__(self):
        # Update this format string when you update states.
        # Format includes padding to next 64-byte boundary,
        # but the pad bytes aren't used on the Python end.
        # It would be better to autogenerate the format string,
        # but I can't think of a simple way to do it without a lot
        # of screweing around.
        self.format = '=IffffffIiiiIIIIffffffffffffffIII'
        self.states = OrderedDict()
        self.states["elapsed"] = 0
        self.states["yaw"] = 0.0
        self.states["pitch"] = 0.0
        self.states["roll"] = 0.0
        self.states["yaw_rate"] = 0.0
        self.states["pitch_rate"] = 0.0
        self.states["roll_rate"] = 0.0
        self.states["throttle_collective"] = 0
        self.states["throttle_yaw_offset"] = 0
        self.states["throttle_pitch_offset"] = 0
        self.states["throttle_roll_offset"] = 0
        self.states["command_yaw"] = 0
        self.states["command_pitch"] = 0
        self.states["command_roll"] = 0
        self.states["command_collective"] = 0
        self.states["command_heading"] = 0.0
        self.states["compensator_e_yaw"] = 0.0
        self.states["compensator_p_yaw"] = 0.0
        self.states["compensator_d_yaw"] = 0.0
        self.states["compensator_e_pitch"] = 0.0
        self.states["compensator_p_pitch"] = 0.0
        self.states["compensator_d_pitch"] = 0.0
        self.states["compensator_e_roll"] = 0.0
        self.states["compensator_p_roll"] = 0.0
        self.states["compensator_d_roll"] = 0.0
        self.states["compensator_K_p"] = 0.0
        self.states["compensator_K_d"] = 0.0
        self.states["compensator_K_p_yaw"] = 0.0
        self.states["compensator_K_d_yaw"] = 0.0
        self.states["imu_bad_checksums"] = 0
        self.states["imu_ticks"] = 0
        self.states["imu_samples_in_buffer"] = 0
        
    def __getitem__(self, key):
        if key not in self.states.keys():
            raise UnknownStateError('Unknown state: ', key)
        return self.states[key]

    def __setitem__(self, key, item):
        if key not in self.states.keys():
            raise UnknownStateError('Unknown state: ', key)
        self.states[key] = item
    
    def keys(self):
        return copy.copy(self.states.keys())
    
    def values(self):
        return self.states.values()

    def to_string(self):
        vals = self.states.values()
        str = ""
        for i in range(len(vals)):
            str += struct.pack('=' + self.format[i+1], vals[i])
        return str
        
    @classmethod
    def from_string(cls, str):
        state = AirframeState()
        fmt = state._get_format_string()
        values = struct.unpack(fmt, str)
        i = 0
        for key in state.keys():
            state.states[key] = values[i]
            i+=1
        return state 

    def _get_format_string(self):
        return self.format
        

# test jig
if __name__ == '__main__':
    # An AirframeState knows how big it is when pickled.
    # Note: we're assuming that this number is the same on
    # all implementations. Tested on Python 2.7 (Win32) and
    # Python 2.6 (Gumstix Linux).
    s = AirframeState()
    n = s.pickled_len()
    print "pickled_len() returned ", n
    assert n != 0

    # An AirframeState implements basic Dictionary methods,
    # get, set, keys, values:
    k = len(s.keys())
    print ""
    print "key count: ", k
    print s.keys()
    
    v = len(s.values())
    print ""
    print "value count: ", v
    print s.values()
    assert k == v

    print ""
    print "setting all values to 1.0 and reading them out"
    for key in s.keys():
        s[key] = 1.0    
    print s.values()
    for value in s.values():
        assert value == 1.0

    # The length of a pickled AirframeState should remain constant
    del s
    s = AirframeState()
    ps = cPickle.dumps(s)
    len_before = len(ps)
    for key in s.keys():
        t = type(s[key])
        s[key] = t(2.0)
    del ps
    ps = cPickle.dumps(s)
    len_after = len(ps)
    assert len_before == len_after

    # Measure the time it takes to pickle and unpickle an AirframeState
    del s
    del ps
    s = AirframeState()
    n = 10000
    start = time.clock()    
    for i in range(n):
        ps = cPickle.dumps(s)
    end = time.clock()
    pps = n / (end - start)
    print pps, "pickles per second"
    
    del s
    del ps
    s = AirframeState()
    n = 10000
    ps = cPickle.dumps(s)
    start = time.clock()    
    for i in range(n):
        ups = cPickle.loads(ps)
    end = time.clock()
    pps = n / (end - start)
    print pps, "un-pickles per second"
    
    # attempt to get or set a non-existent state (should fail)
    del s
    s = AirframeState()
    try:
        s['some_new_thing'] = "some new thing"
    except UnknownStateError as e:
        print "Couldn't add a new state ", e.state, "...msg: ", e.msg 
    
    try:
        print s['some_new_thing']
    except UnknownStateError as e:
            print "Couldn't retrieve non-existent state: ", e.state

    # An AirframeState can pack itself into a string representation and back.
    state_in = AirframeState()
    state_in["elapsed"] = 9.0
    str = state_in.to_string()
    state_out = AirframeState.from_string(str)
    for state in state_in.states:
        assert(type(state_in.states[state]) == type(state_out.states[state]))
    
    assert(state_out["elapsed"] == 9.0)
    
    print "\nAll tests passed."



