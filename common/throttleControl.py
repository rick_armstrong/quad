# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import cksum
from cksum import cksum
import pdb
from pdb import set_trace
import struct

# Pololu Micro Maestro takes servo commands in
# quarter-microseconds
SERVO_MAX = 2240 * 4
SERVO_MIN = 1000 * 4
SERVO_RANGE = SERVO_MAX - SERVO_MIN

def makeCommandString(pulse_width_quarter_ns, motor_index):
    cmdString = ""
    low = pulse_width_quarter_ns & 0x7f
    high = (pulse_width_quarter_ns >> 7) & 0x7f
    cmdString = chr(0x84) + chr(motor_index) + chr(low) + chr(high)
    return cmdString

def set_throttle(throttleList, port):
    for i in range(4):
        throttle =  SERVO_MIN + ((throttleList[i] / 255.0) * SERVO_RANGE)
        cmd = makeCommandString(int(throttle), i)
        port.write(cmd)
if __name__ == '__main__':
    pass