#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Testing suggests that 30 Hz is the max loop rate. See
# microstrain_3dm-gx1_test.py for a demo. If we poll as fast as we can,
# the timestamps accompanying the data are 5 ticks apart. One tick is
# 6.5536 ms. 6.5536 * 5 = 32.768 => 30.5 Hz.
# In streaming mode, the default output rate is 76 Hz.
CONTROL_LOOP_RATE_HZ = 76
CONTROL_LOOP_PERIOD_S = 1.0 / CONTROL_LOOP_RATE_HZ

CONTROL_PORT = 4444
CONFIG_PORT = 4445
LOG_BROADCAST_PORT = 4446
LOG_BROADCAST_ADDR = '192.168.4.255'
LOG_BROADCAST_LISTEN_IF = '192.168.4.126'
COMMAND_SERVER_LISTEN_IF = '192.168.4.126'
CONTROL_MSG_LEN = 16
CONTROL_MSG_ID = 0
CONFIG_MSG_LEN = 16
CONFIG_MSG_ID = 1
COMMAND_SERVER_SOCKET_TIMEOUT_s = 5.0

MAX_THROTTLE_OFFSET = 127.0
MAX_TILT_DEGREES = 127.0
MAX_TILT_CMD_DEGREES = 30.0

###### PID control constants
# As of 2012-08-01, 0.5, 10.0 is very well-behaved
K_p = 0.5
K_d = 0.2
K_d_yaw = 2.0

###### PDF control constants
K_i = 0.00
K_d1 = 0.0
K_d2 = 0.0

# This should be set to true when we're 
# running on the actual quad hardware
# so we don't create an ever-growing 
# log file. 
IMU_DISABLE_LOGFILE = True

# Microstrain 3DM-GX1
IMU_PACKET_HEADER = 0x31 # 'Gyro-stabilized Euler, Accel, Rate Vector'
#IMU_PORT_IDX = 0
#IMU_PORT_STR = '/dev/ttyS2'
#IMU_PORT_BAUD = 38400 # device default
#IMU_PORT_TIMEOUT_s = 1

IMU_PORT_IDX = 0
IMU_PORT_STR = 'COM1'
IMU_PORT_BAUD = 38400 # device default
IMU_PORT_TIMEOUT_s = 1

# Pololu Micro Maestro Servo Controller settings
LL_CONTROLLER_PORT_IDX = 0
LL_CONTROLLER_PORT_STR = '/dev/ttyS0'
LL_CONTROLLER_PORT_BAUD = 48000 
LL_CONTROLLER_PORT_TIMEOUT_s = 1

# Starken Industries LL Controller settings
#LL_CONTROLLER_PORT_IDX = 0
#LL_CONTROLLER_PORT_STR = 'COM1'
#LL_CONTROLLER_PORT_BAUD = 57600 
#LL_CONTROLLER_PORT_TIMEOUT_s = 1