//	Copyright (c) 2012, Rick Armstrong
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//			modification, are permitted provided that the following conditions are met:
//
//	* Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//
//	* Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	* Neither the name of the copyright holder nor the names of its
//	contributors may be used to endorse or promote products derived from
//	this software without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//			IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
//			FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//			DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//			SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
//			CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#ifndef AIRFRAME_STATE_H_
#define AIRFRAME_STATE_H_
#endif // AIRFRAME_STATE_H_

typedef struct AirframeState_tag {
	unsigned elapsed; // microseconds
	float yaw;
	float pitch;
	float roll;
	float yaw_rate;
	float pitch_rate;
	float roll_rate;
	unsigned throttle_collective;
	int throttle_yaw_offset;
	int throttle_pitch_offset;
	int throttle_roll_offset;
	unsigned command_yaw;
	unsigned command_pitch;
	unsigned command_roll;
	unsigned command_collective;
	float command_heading;
	float compensator_e_yaw;
	float compensator_p_yaw;
	float compensator_d_yaw;
	float compensator_e_pitch;
	float compensator_p_pitch;
	float compensator_d_pitch;
	float compensator_e_roll;
	float compensator_p_roll;
	float compensator_d_roll;
	float compensator_K_p;
	float compensator_K_d;
	float compensator_K_p_yaw;
	float compensator_K_d_yaw;
	unsigned imu_bad_checksums;
	unsigned imu_ticks;
	unsigned imu_samples_in_buffer;

	// pad to next 64-byte boundary
//	char pad[0];
} AirframeState;