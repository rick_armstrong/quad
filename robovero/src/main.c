//	Copyright (c) 2012, Rick Armstrong
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//			modification, are permitted provided that the following conditions are met:
//
//	* Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer.
//
//	* Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution.
//
//	* Neither the name of the copyright holder nor the names of its
//	contributors may be used to endorse or promote products derived from
//	this software without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//			IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
//			FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//			DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//			SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
//			CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/*******************************************************************************
  Quadrotor controller, based on factory Robovero firmware.
 ******************************************************************************/
#include <math.h>

#include "message.h"
#include "return.h"
#include "table.h"

#include "LPC17xx.h"
#include "lpc_types.h"
#include "lpc17xx_adc.h"
#include "lpc17xx_can.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_pwm.h"
#include "lpc17xx_timer.h"
#include "lpc17xx_uart.h"

#include "airframe_state.h"

float unwrap_degrees_180(float d);

/*****************************************************************************
  Globals and constants
*****************************************************************************/
unsigned g_cmd[6] = {1500, 1500, 1000, 1500, 1000, 1000}; // commands from RC Xmitter
unsigned Ticks = 0;
const int MAX_PPM = 2000; // microseconds
const int MIN_PPM = 1000; // microseconds
const int PPM_RANGE = 2000 - 1000; // MAX_PPM - MIN_PPM
const int PPM_MIDPOINT = 1500; // MIN_PPM + (PPM_RANGE / 2.0)
const float MAX_TILT_CMD = 30.0; // degrees
const float MAX_YAW_CMD = 200.0; // 3.1415 / 2.0; // rads / second
const float HEADING_SETPOINT_CMD_PRESCALE = 3.0; // rate of heading setpoint change
const float HEADING_CMD_DEADBAND = 10.0 / 500.0; // 10/500 PPM counts
const float IMU_SAMPLE_RATE_HZ = 76.0;

// these are specified in the "new" units, microseconds/degree
float K_p = 3.0;
float K_p_yaw = 3.0; 
float K_d = 50.0;
float K_d_yaw = 10.0; 
const char MOTOR_COUNT = 4;

/*****************************************************************************
  static
*****************************************************************************/
static unsigned char e_stop = 0;
static float command_heading = 0.0;

/*****************************************************************************

  Generic Interrupt Service Routine

*****************************************************************************/
void IntHandler(void)
{
  unsigned int int_num;
//  uint8_t int_str[8];

  /*
   * Get the interrupt number
   */
  __asm("mrs %0, ipsr;" : "=r"(int_num) );
  int_num -= 16;

  /*
   * Disable the interrupt
   */
  NVIC_DisableIRQ(int_num);

  /*   
   * Send the interrupt signal and number
   */
//  sprintf((char*) int_str, "\r\n%x\r\n", int_num);
//  writeUSBOutString((uint8_t*)int_str);
}

/*****************************************************************************

    Hardware Initialization Routine

*****************************************************************************/

void hwInit(void)
{
  /*
   * make the LED pin an output and turn it off
   */
  GPIO_SetDir(3, (1 << 25), 1);
  GPIO_SetValue(3, (1 << 25));

  /*
   * start the usb device wait until configuration completes before proceeding
   */
  USB_Init();
  USB_Connect(TRUE);
  while (!USB_Configuration);
}

void toggleLED()
{
  static int led_state = 0;
  if(1 == led_state)
  {
    GPIO_ClearValue(3, (1 << 25));
    led_state = 0;
  }
  else
  {
    GPIO_SetValue(3, (1 << 25));
    led_state = 1;
  }
}

void initMatch(uint8_t channel, uint32_t value, uint8_t enable)
{
  PWM_MATCHCFG_Type PWMMatchCfgDat;
  
  PWM_MatchUpdate(LPC_PWM1, channel, value, PWM_MATCH_UPDATE_NOW);
  PWMMatchCfgDat.IntOnMatch = DISABLE;
  PWMMatchCfgDat.MatchChannel = channel;
  if (!channel)
  {
    PWMMatchCfgDat.ResetOnMatch = ENABLE;
  }
  else
  {
    PWMMatchCfgDat.ResetOnMatch = DISABLE;
    PWMMatchCfgDat.StopOnMatch = DISABLE;
    PWM_ConfigMatch(LPC_PWM1, &PWMMatchCfgDat);
  }
  if(enable)
  {
    PWM_ChannelCmd(LPC_PWM1, channel, ENABLE);    
  }
}

void TIMER0_IRQHandler(void)
{
  static uint8_t synched = 0;
  static uint8_t curChan = 0;
  
  // capture and clear timer
  TIM_ClearIntCapturePending(LPC_TIM0,1);
  uint32_t t = TIM_GetCaptureValue(LPC_TIM0,1);
  TIM_ResetCounter(LPC_TIM0);
  
  // Is this a sync pulse?
  // Note: setting this value too low (e.g. 2000) causes mayhem (see
  // notes on "Clusterfuck Mode".
  if(5000 < t)
  {
    synched = 1;
    curChan = 0;
    return;
  }
  
  if(!synched)
  {
    return;
  }
  
  g_cmd[curChan] = t;  
  curChan++;
  
  // this shouldn't happen unless we get spurious pulses
  if(6 < curChan)
  {
    // reset and move on
    synched = 0;
  }
}

void quadconfig()
{
  // configure pin 1.27 for timer capture
  PINSEL_CFG_Type PinCfg;
  PinCfg.Funcnum = 3;
  PinCfg.OpenDrain = 0;
  PinCfg.Pinmode = 0;
  PinCfg.Portnum = 1;
  PinCfg.Pinnum = 27;
  PINSEL_ConfigPin(&PinCfg);

  TIM_TIMERCFG_Type TIM_ConfigStruct;
  TIM_CAPTURECFG_Type TIM_CaptureConfigStruct;
  
    // Initialize timer 0, prescale count time of 1uS
  TIM_ConfigStruct.PrescaleOption = TIM_PRESCALE_USVAL;
  TIM_ConfigStruct.PrescaleValue  = 1;
  
    // use channel 0, CAPn.1
    // Enable capture on CAPn.1 falling edge
    // Generate capture interrupt
  TIM_CaptureConfigStruct.CaptureChannel = 1;
  TIM_CaptureConfigStruct.FallingEdge = ENABLE;
  TIM_CaptureConfigStruct.RisingEdge = DISABLE;  
  TIM_CaptureConfigStruct.IntOnCaption = ENABLE;  
  
    // Set configuration for Tim_config
  TIM_Init(LPC_TIM0, TIM_TIMER_MODE,&TIM_ConfigStruct);
  TIM_ConfigCapture(LPC_TIM0, &TIM_CaptureConfigStruct);
  TIM_ResetCounter(LPC_TIM0);
  
    // preemption = 1, sub-priority = 1
    // enable interrupt for timer 0
    // start timer 0
  NVIC_SetPriority(TIMER0_IRQn, ((0x01<<3)|0x01));
  NVIC_EnableIRQ(TIMER0_IRQn);
  TIM_Cmd(LPC_TIM0,ENABLE);
  
  // configure PWM
    // 20000 us = 20 ms = 50 Hz  
  initMatch(0, 2500, 0);
  
    // ch.1 defaults to single-edge
  PWM_ChannelConfig(LPC_PWM1, 2, PWM_CHANNEL_SINGLE_EDGE);  
  PWM_ChannelConfig(LPC_PWM1, 3, PWM_CHANNEL_SINGLE_EDGE);  
  PWM_ChannelConfig(LPC_PWM1, 4, PWM_CHANNEL_SINGLE_EDGE);  
  PWM_ChannelConfig(LPC_PWM1, 5, PWM_CHANNEL_SINGLE_EDGE);  
  PWM_ChannelConfig(LPC_PWM1, 6, PWM_CHANNEL_SINGLE_EDGE);
  
    // set all to min and enable all
  initMatch(1, 500, 1);
  initMatch(2, 500, 1);
  initMatch(3, 500, 1);
  initMatch(4, 500, 1);
  initMatch(5, 500, 1);
  initMatch(6, 500, 1);

    // start PWM
  PWM_ResetCounter(LPC_PWM1);
  PWM_CounterCmd(LPC_PWM1, ENABLE);
  PWM_Cmd(LPC_PWM1, ENABLE);
  
  // setup UART2
  UART_CFG_Type UARTConfigStruct;  
  UART_ConfigStructInit(&UARTConfigStruct);  
  UARTConfigStruct.Baud_rate = 38400;
  UART_Init(LPC_UART2, &UARTConfigStruct);
  UART_FIFO_CFG_Type UARTFIFOConfigStruct;  
  UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);
  UART_FIFOConfig(LPC_UART2, &UARTFIFOConfigStruct);

  TIM_Init(LPC_TIM1, TIM_TIMER_MODE,&TIM_ConfigStruct);
  TIM_ResetCounter(LPC_TIM1);
  TIM_Cmd(LPC_TIM1,ENABLE);
}

int16_t to_signed(uint16_t n)
{
  if(32767 < n)
  {
    return n - 65535;
  }
  else
  {
    return n;
  }
}

float signum(float x)
{
  if (x < 0.0)
  {
    return -1;
  }
  return 1;
}

int cksum( unsigned char* buffer, int length) {
  int checkSum = 0;
  int i = 0;
  for(i = 0; i < (length); i = i + 2)
  {
    checkSum += (buffer[i] * 256) + buffer[i + 1];
  }
  return(checkSum & 0xFFFF);
}

/* Read the IMU and validate the incoming packet.
 * Returns: 0, if the packet passes checksum, -1 if not
  0x31 = "Send Gyro-Stabilized Euler Angles & Accel & Rate Vector"
  23 bytes:
  Byte 1 Header byte = 0x31
  Byte 2 Roll MSB
  Byte 3 Roll LSB
  Byte 4 Pitch MSB
  Byte 5 Pitch LSB
  Byte 6 Yaw MSB
  Byte 7 Yaw LSB
  Byte 8 Accel_X MSB
  Byte 9 Accel_X LSB
  Byte 10 Accel_Y MSB
  Byte 11 Accel_Y LSB
  Byte 12 Accel_Z MSB
  Byte 13 Accel_Z LSB
  Byte 14 CompAngRate_X MSB
  Byte 15 CompAngRate_X LSB
  Byte 16 CompAngRate_Y MSB
  Byte 17 CompAngRate_Y LSB
  Byte 18 CompAngRate_Z MSB
  Byte 19 CompAngRate_Z LSB
  Byte 20 TimerTicks MSB
  Byte 21 TimerTicks LSB
  Byte 22 Checksum MSB
  Byte 23 Checksum LSB
*/
int8_t read_imu(AirframeState* airframe_state)
{
  uint8_t c = 0;
  uint32_t len = 0;
  uint8_t buf[24];
  memset(buf, 0, sizeof(buf));
  buf[1] = 0x31;

  // wait for the header
  uint8_t synched = 0;
  while(!synched)
  {
    len = UART_Receive(LPC_UART2, &c, 1, BLOCKING);
    if(0x31 == c)
    {
      synched = 1;
    }
  }
  synched = 0;
  
  // get the rest of the message
  len = UART_Receive(LPC_UART2, buf + 2, 22, BLOCKING);
  
    // in degrees
  airframe_state->yaw = to_signed((buf[6] * 256) + buf[7]) * (360.0 / 65535);  
  airframe_state->pitch = to_signed((buf[4] * 256) + buf[5]) * (360.0 / 65535);
  airframe_state->roll = to_signed((buf[2] * 256) + buf[3]) * (360.0 / 65535);
  
    // in rad/s
  airframe_state->yaw_rate = to_signed((buf[18] * 256) + buf[19]) / 3276.8;
  airframe_state->pitch_rate = to_signed((buf[16] * 256) + buf[17]) / 3276.8;
  airframe_state->roll_rate = to_signed((buf[14] * 256) + buf[15]) / 3276.8;
  
  // validate the incoming packet
  unsigned short cksum_recvd = (buf[22] * 256 + buf[23]);
  unsigned short cksum_calc = cksum_calc = cksum(buf, 22);
  return (cksum_recvd == cksum_calc) ? 0 : -1;
}

void get_command(AirframeState* airframe_state)
{
  airframe_state->command_yaw = g_cmd[3];
  airframe_state->command_pitch = g_cmd[1];
  airframe_state->command_roll = g_cmd[0];
  airframe_state->command_collective = g_cmd[2];
}

float tilt_cmd(unsigned cmd)
{
  // normalize to [-1.0, 1.0] and scale
  float cmd_norm = (cmd / ((MAX_PPM - MIN_PPM) / 2.0)) - 3.0;
  return cmd_norm * MAX_TILT_CMD;
}

// see unwrap.py for the python version and associated unit tests
float calc_e_yaw(AirframeState* state)
{
  float e_yaw = 0.0;
  float e = state->command_heading - state->yaw;
  if(abs(e) > 180.0)
  {
    e_yaw = -signum(e) * (360.0 - abs(e));
  }
  else
  {
    e_yaw = e;
  }
  return e_yaw;
}

float update_command_heading(unsigned cmd)
{
  // normalize [MIN_PPM, MAX_PPM] to [-1.0, 1.0]
  float cmd_norm = ((float) cmd) - PPM_MIDPOINT; // translate
  cmd_norm = cmd_norm / PPM_RANGE / 2.0; // scale
  
  if(fabs(cmd_norm) < HEADING_CMD_DEADBAND)
  {
    return command_heading;   
  }
  
  // this is negative because we're using the NED coordinate frame
  // i.e., 'z' is down
  command_heading -= cmd_norm * HEADING_SETPOINT_CMD_PRESCALE;
  command_heading = unwrap_degrees_180(command_heading);
  return command_heading;  
}

float degrees_to_radians(float d)
{
  return d * 3.1415 / 180.0;
}

void update(AirframeState* state)
{
  // throttle scalar to compensate for the thrust vector tilt and the fact
  // that thrust is proportional to the square of rotor speed
  float thrust_comp =
  ((cos(degrees_to_radians(fabs(state->roll)))) *
  (cos(degrees_to_radians(fabs(state->pitch)))));
  thrust_comp = sqrt(thrust_comp);
  
  // set baseline throttle and heading values
  state->throttle_collective = (float)state->command_collective / thrust_comp;
  state->command_heading = update_command_heading(state->command_yaw);
  
  // error values, reference minus plant measurement
  // (signs derived from joystick configured for an airplane)
  float e_yaw = calc_e_yaw(state);
  float e_pitch = tilt_cmd(state->command_pitch) - state->pitch;
  float e_roll = tilt_cmd(state->command_roll) - state->roll;
  
  // P
  float p_yaw = e_yaw * K_p_yaw;
  float p_pitch = e_pitch * K_p;
  float p_roll = e_roll * K_p;
  
  // D
  float d_yaw = -state->yaw_rate * K_d_yaw;
  float d_pitch = -state->pitch_rate * K_d;
  float d_roll = -state->roll_rate * K_d;
  
  // store PID internals
  state->compensator_e_yaw = e_yaw;
  state->compensator_p_yaw = p_yaw;
  state->compensator_d_yaw = d_yaw;
  state->compensator_e_pitch = e_pitch;
  state->compensator_p_pitch = p_pitch;
  state->compensator_d_pitch = d_pitch;     
  state->compensator_e_roll = e_roll;
  state->compensator_p_roll = p_roll;
  state->compensator_d_roll = d_roll;
  state->compensator_K_p = K_p;
  state->compensator_K_d = K_d;
  state->compensator_K_p_yaw = K_p_yaw;
  state->compensator_K_d_yaw = K_d_yaw;

  state->throttle_roll_offset = p_roll + d_roll;
  state->throttle_pitch_offset = p_pitch + d_pitch;
  state->throttle_yaw_offset = p_yaw + d_yaw;
}

unsigned clamp(unsigned x)
{
  if(x < MIN_PPM)
  {
      return MIN_PPM;
  }

  if(x > MAX_PPM)
  {
      return MAX_PPM;
  }
  return x;
}

void set_throttles(AirframeState* state)
{  
  // throttle values (microseconds)
  unsigned t[MOTOR_COUNT];

  int i;

  // collective
  for(i = 0; i < MOTOR_COUNT; ++i)
  {
    t[i] = state->throttle_collective;
  }
  
  // yaw
  t[0] -= state->throttle_yaw_offset;
  t[1] += state->throttle_yaw_offset;
  t[2] -= state->throttle_yaw_offset;
  t[3] += state->throttle_yaw_offset;
  
  // pitch
  t[0] += state->throttle_pitch_offset;
  t[2] -= state->throttle_pitch_offset;
  
  // roll
  t[1] -= state->throttle_roll_offset;
  t[3] += state->throttle_roll_offset;

  // safety cutoff
  // TODO: fix hard-coded min throttle
  if((1150 > state->command_collective) || e_stop)
  {
    for(i = 0; i < MOTOR_COUNT; ++i)
    {
      t[i] = 1000;
    }
  
    // if the safety cutoff is engaged, we're on the ground,
    // so it's a nice time to reset command_heading to the
    // direction we're currently pointing
    command_heading = state->yaw;
  }
  
  for(i = 0; i < MOTOR_COUNT; ++i)
  {
    PWM_MatchUpdate(LPC_PWM1, i + 1, clamp(t[i]), PWM_MATCH_UPDATE_NEXT_RST);  
  }
}

void set_PID_params(float p, float i, float d, float d_yaw)
{
  K_p = p;
  K_d = d;
  K_d_yaw = d_yaw;
}


//   Convert an angle in [0.0, 360.0] degrees to [-180.0, 180.0]
float unwrap_degrees_180(float d)
{
  float zero_to_twopi = fmod(d, 360.0);
  if(-1 == signum(zero_to_twopi))
  {
    zero_to_twopi += 360.0;
  }
  
  if((zero_to_twopi >= 0.0) && (zero_to_twopi <= 180.0))
  {
    return zero_to_twopi;
  }
  else
  {
    return zero_to_twopi - 360.0;
  }
}

void initialize_yaw(AirframeState* airframe_state)
{
  read_imu(airframe_state);
  command_heading = airframe_state->yaw;  
}

void calc_average__airframe_state(AirframeState* avg, AirframeState* history)
{
  avg->yaw = (history[0].yaw + history[1].yaw) / 2.0;
  avg->pitch = (history[0].pitch + history[1].pitch) / 2.0;
  avg->roll = (history[0].roll + history[1].roll) / 2.0;
  avg->yaw_rate = (history[0].yaw_rate + history[1].yaw_rate) / 2.0;
  avg->pitch_rate = (history[0].pitch_rate + history[1].pitch_rate) / 2.0;
  avg->roll_rate = (history[0].roll_rate + history[1].roll_rate) / 2.0;  
}

int main(void)
{
  // Generic init functions from Robovero factory firmware
  // (config pins in accordance with PCB markings)
  hwInit();
  _roboveroConfig(0);
  GPIO_ClearValue(3, (1 << 25)); // turn on an LED so we know someone's home

  // quad-specific configuration: PPM in, IMU in, PWM out
  quadconfig();
  
  ///////////////////////////////////////////////////////////////////////////
  // FLY!!! (main control loop)
  //
  AirframeState* airframe_state;
  airframe_state = malloc(sizeof(AirframeState));
  memset(airframe_state, 0, sizeof(AirframeState));
  AirframeState airframe_state_history[2];  

  initialize_yaw(airframe_state);

  unsigned loop_count = 0;
  while(1)
  {
    get_command(airframe_state);
    if(0 > read_imu(airframe_state))
    {
      // checksum error
      toggleLED();
    }
    memcpy(&(airframe_state_history[loop_count % 2]), airframe_state, sizeof(AirframeState));
   
    // safety cuttof
    if(((60.0 < abs(airframe_state->roll)) ||
    (60.0 < abs(airframe_state->pitch))) &&
    !e_stop)
    {
      // set_throttles() will see this and kill motors.
      // Only a hard reset will re-enable the motors.
      e_stop = 1;
    }
    if(e_stop && ((loop_count % 76) == 0))
    {
      toggleLED();
    }
   
    if(loop_count % 2 == 0)
    {
      calc_average__airframe_state(airframe_state, airframe_state_history);
      update(airframe_state);
      set_throttles(airframe_state);
    }
    uint32_t t = LPC_TIM1->TC; // read timer 1
    TIM_ResetCounter(LPC_TIM1);
    TIM_Cmd(LPC_TIM1,ENABLE);
    airframe_state->elapsed += t;
    airframe_state->imu_ticks = Ticks++;
    writeUSBOut((uint8_t*) airframe_state, sizeof(AirframeState));
    loop_count++;
  }

  return 0;
}














