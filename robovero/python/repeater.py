#!/usr/bin/env python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pdb
import serial
import struct
import sys
import time

from HL_controller.hl_controller import UDPAirframeStateBroadcaster
from common.airframe_state import AirframeState

packet_len = struct.calcsize(AirframeState().format)

def unpack_test():
	p = serial.Serial('/dev/ttyACM0', 115200, timeout=0.001)
	p.flushInput()
	read_count = 0
	timeout_count = 0
	t0 = time.time()
	unpacked = []
	yaw_history = []
	pitch_history = []
	roll_history = []
	command_heading_history = []
	try:	
		while(1):
			data = p.read(packet_len)
			if len(data) == packet_len:
				read_count += 1
				unpacked = struct.unpack(AirframeState().format, data)
				yaw_history.append(unpacked[1])
				pitch_history.append(unpacked[2])
				roll_history.append(unpacked[3])
				command_heading_history.append(unpacked[15])
			else:
				timeout_count += 1
			
	except KeyboardInterrupt:
		t1 = time.time()
		t = t1 - t0		
		p.close()
		print ""
		print read_count, " reads in ", str(t), "seconds = ", str(read_count / t), " Hz"
		print "timeout_count: ", timeout_count
		y = np.array(yaw_history)
		p = np.array(pitch_history)
		r = np.array(roll_history)
		h = np.array(command_heading_history)
		if len(y) > 0:
			print "average ypr: ", np.mean(y), np.mean(p), np.mean(r)
			print "std ypr: ", np.std(y), np.std(p), np.std(r)
#			plt.plot(r, label="roll")
			plt.plot(y, label="yaw")
#			plt.plot(p, label="pitch")
#			plt.plot(K_p, label="K_p")
#			plt.plot(K_d, label="K_d")
#			plt.plot(K_d_yaw, label="K_d_yaw")
			plt.plot(h, label="command_heading")
			plt.legend()
			plt.show()

def repeat():
	p = serial.Serial('/dev/ttyACM0', 115200, timeout=0.001)
	p.flushInput()
	read_count = 0
	timeout_count = 0
	unpacked = []
	yaw_history = []
	pitch_history = []
	roll_history = []
	state = AirframeState()
	broadcaster = UDPAirframeStateBroadcaster()
	broadcaster.start()

	try:
		fmt = AirframeState().format
		while(1):
			data = p.read(packet_len)
			if len(data) == packet_len:
				read_count += 1
				unpacked = struct.unpack(fmt, data)
				state["elapsed"] = unpacked[0]
				state["yaw"] = unpacked[1]
				state["pitch"] = unpacked[2]
				state["roll"] = unpacked[3]
				state["yaw_rate"] = unpacked[4]
				state["pitch_rate"] = unpacked[5]				
				state["roll_rate"] = unpacked[6]
				state["throttle_collective"] = unpacked[7]
				state["throttle_yaw_offset"] = unpacked[8]
				state["throttle_pitch_offset"] = unpacked[9]
				state["throttle_roll_offset"] = unpacked[10]	
				state["command_yaw"] = unpacked[11]
				state["command_pitch"] = unpacked[12]
				state["command_roll"] = unpacked[13]				
				state["command_collective"] = unpacked[14]
				state["compensator_e_yaw"] = unpacked[15]
				state["compensator_p_yaw"] = unpacked[16]
				state["compensator_d_yaw"] = unpacked[17]
				state["compensator_e_pitch"] = unpacked[18]
				state["compensator_p_pitch"] = unpacked[19]
				state["compensator_d_pitch"] = unpacked[20]
				state["compensator_e_roll"] = unpacked[21]
				state["compensator_p_roll"] = unpacked[22]
				state["compensator_d_roll"] = unpacked[23]
				state["compensator_K_p"] = unpacked[24]
				state["compensator_K_d"] = unpacked[25]
				state["compensator_K_d_yaw"] = unpacked[26]				
				state["imu_bad_checksums"] = unpacked[27]
				state["imu_ticks"] = unpacked[28]
				state["imu_samples_in_buffer"] = unpacked[29]
				broadcaster.send(state)

			else:
				timeout_count += 1
			
	except KeyboardInterrupt:
		p.close()
		broadcaster.quit = True
		

if __name__ == '__main__':
	unpack_test()
#	repeat()