# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from collections import deque
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pdb
import serial
import struct
import time


if __name__ == '__main__':
	data = []
	p = serial.Serial('/dev/ttyACM0', 115200)
	p.flushInput()
	try:
		loop_count = 0
		while(True):
			s = p.readline()
			data.append(s)
			loop_count += 1
			if 0 == (loop_count % 76):
				print s
	except KeyboardInterrupt:
		p.close()

		
	#data = np.zeros(1000000)
	#p = serial.Serial('/dev/ttyACM0', 115200)
	#try:
	#	t0 = time.time()
	#	for i in range(1000000):
	#		data[i] = ord(p.read())
	#		if 0 == (i % 1000):
	#			t1 = time.time()
	#			print "1000 chars in ", t1 - t0, " seconds"
	#			t0 = t1
	#except KeyboardInterrupt:
	#	p.close()
	#	plt.plot(data)
	#	plt.show()
	#	pdb.set_trace()
		