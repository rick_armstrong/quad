#!/usr/bin/python
# BSD 3-Clause License
#
# Copyright (c) 2012, Rick Armstrong
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import socket
import time
import threading

TEST_STRING = "something"

g_lock = threading.Lock()

class Broadcaster(threading.Thread):
    def __init__(self, addr, port):
        self.quit = False
        self.addr = addr
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_BROADCAST, 1)
        threading.Thread.__init__(self)

    def run(self):
        while(not self.quit):
            s = TEST_STRING
            self.sock.sendto(s, (self.addr, self.port))
            g_lock.acquire()
            print ""
            print "sent ", TEST_STRING
            g_lock.release()
            time.sleep(3)
        self.sock.close()
        self.sock = None
        
class Listener(threading.Thread):
    def __init__(self, addr, port):
        self.quit = False
        self.addr = addr
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.sock.settimeout(5.0)
        self.sock.bind((self.addr, self.port))        
        threading.Thread.__init__(self)
    
    def run(self):
        while(not self.quit):
            try:
                (s, addr) = self.sock.recvfrom(len(TEST_STRING))
                g_lock.acquire()
                print "got ", s
                print ""
                g_lock.release()
            except socket.timeout:
                print "listener socket timeout"
                if(self.quit == True):
                    break
                continue
        self.sock.close()
        self.sock = None
        
def main():

    try:
        print "starting listener..."
        listener = Listener('127.0.0.1', 4444)
        listener.start()
        
        print "starting broadcaster..."
        broadcaster = Broadcaster('127.0.0.1', 4444)
        broadcaster.start()
                          
        while(True):
            time.sleep(1)
            
    except KeyboardInterrupt:
        print "waiting for broadcaster to quit..."
        broadcaster.quit = True
        broadcaster.join()
        
        print "waiting for listener to quit..."
        listener.quit = True
        listener.join()
        
        print "Exiting."
        
if __name__ == "__main__":
    main()